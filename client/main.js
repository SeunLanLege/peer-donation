import { FlowRouter } from 'meteor/kadira:flow-router'
import { Meteor } from 'meteor/meteor'

import '../imports/startup/client'
import UserAutorun from '../imports/autoruns/UserAutorun'

FlowRouter.wait()

Meteor.startup(() => {
  UserAutorun.start()
  FlowRouter.initialize()
})
