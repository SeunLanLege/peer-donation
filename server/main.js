import { Meteor } from 'meteor/meteor'
import { SyncedCron } from 'meteor/percolate:synced-cron'

import '../imports/startup/server/main'

Meteor.startup(() => {
  process.env.MAIL_URL = 'smtp://peerdonations90@gmail.com:Password@90@smtp.gmail.com:587'
  SyncedCron.start()
})
