//  @flow
import { Meteor } from 'meteor/meteor'
import Dash from '../store/DashboardStore'
import User from '../store/User'
import { Donor } from '../Classes/Donor'
import { Beneficiary } from '../Classes/Beneficiary'
import { DonationPackages } from '../Classes/Packages'

import autorun from 'meteor/space:tracker-mobx-autorun'

const DashAutorun = () => {
  if (User.userId) {
    const handleDonations = Meteor.subscribe('user.donations')
    const handleWithdrawals = Meteor.subscribe('user.withdrawals')
    const handlePackages = Meteor.subscribe('packages')

    if (User.isKanye) {
      const images = Meteor.subscribe('images.all')
      Dash.toggleLoading('isImagesLoading', !images.ready())
    }

    if (handlePackages.ready()) {
      Dash.toggleLoading('isDonationPackageLoading', !handlePackages.ready())
      Dash.setDonationPackages(DonationPackages.find().fetch())
    }

    if (handleDonations.ready()) {
      Dash.toggleLoading('isDonationLoading', !handleDonations.ready())
      Meteor.user().isKanye
        ? Dash.setDonations(Donor.find().fetch())
        : Dash.setDonations(Donor.find({ donor: User.userId }).fetch())
    }

    if (handleWithdrawals.ready()) {
      Dash.toggleLoading('isWithdrawalLoading', !handleWithdrawals.ready())
      Meteor.user().isKanye
        ? Dash.setWithdrawals(Beneficiary.find().fetch())
        : Dash.setWithdrawals(Beneficiary.find({ beneficiary: User.userId }).fetch())
    }

	
  }
}

export default autorun(DashAutorun)
