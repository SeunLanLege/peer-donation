import { FlowRouter } from 'meteor/kadira:flow-router'
import { Meteor } from 'meteor/meteor'
import User from '../store/User'
import Users from '../store/Users'
import { Statistics } from '../Classes/Statistics'
import autorun from 'meteor/space:tracker-mobx-autorun'

const UserAutorun = () => {
  const userSubscription = Meteor.subscribe('user')
	const handleStats = Meteor.subscribe('statistics')

		if (handleStats.ready()) {
			const uStats = Statistics.findOne()
			User.stats(uStats)
		}

  User.updateUserId(Meteor.userId() && Meteor.userId())
  FlowRouter.watchPathChange()
  User.updateRoute(FlowRouter.current())
  User.updateData('isLoading', !userSubscription.ready())
  if (User.userId && userSubscription.ready()) {
    if (Meteor.user() && Meteor.user().isKanye) {
      User.setIsKanye(Meteor.user() && Meteor.user().isKanye)
      Users.setUsers(Meteor.users.find().fetch())
    } else {
      if (Meteor.userId()) {
        const data = Meteor.users.findOne(User.userId)
        User.setIsKanye(Meteor.user() && Meteor.user().isKanye)
        User.updateData('bankAccName', data.bankAccName)
        User.updateData('bankName', data.bankName)
        User.updateData('bankAccNumber', data.bankAccNumber)
        User.updateData('email', data.emails[0].address)
        User.updateData('phone', data.phone)
        User.updateData('fullName', data.fullName)
      }
    }
  }
}

export default autorun(UserAutorun)
