import { SyncedCron } from 'meteor/percolate:synced-cron'
import { Beneficiary } from '../../Classes/Beneficiary'
import { Waiting } from '../../Classes/Waiting'

export const newBeneficiary = ({ _id, userId, packageId, dueDate }) => {
  if (typeof window === 'undefined') {
    SyncedCron.add({
      name: _id,
      schedule (parser) {
        return parser.recur().on(new Date(dueDate)).fullDate()
      },
      job () {
        const benef = new Beneficiary({ packageId, beneficiary: userId })
        benef.amountLeft = benef.package.reward
        benef.save()
        SyncedCron.remove(_id)
        Waiting.remove(_id)
        return _id
      }
    })
  }
}
