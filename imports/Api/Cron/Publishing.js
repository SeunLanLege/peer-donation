import { SyncedCron } from 'meteor/percolate:synced-cron'
import { Donor } from '../../Classes/Donor'
import { Beneficiary } from '../../Classes/Beneficiary'


SyncedCron.add({
  name: 'publishing cron',
  schedule (parser) {
    return parser.text('at 6:39pm except on Fri, Sat, Sun')
  },
  job () {
    const donor = Donor.find({ isPublished: false, isMatched: true, amountLeft: 0, isCompleted: false })
    donor.forEach((d) => {
      let testArray = []
      testArray = d.beneficiaries.filter((el) => {
        const benef = Beneficiary.findOne(el.benefTransId)
        return benef ? benef.isMatched : false
      })
      if (testArray.length === d.beneficiaries.length) {
        d.beneficiaries.forEach((el) => {
          const benef = Beneficiary.findOne(el.benefTransId)
          if (benef) {
            benef.isPublished = true
            benef.save()
          }
        })
        d.isPublished = true
        d.save()
      }
    })
  }
})
