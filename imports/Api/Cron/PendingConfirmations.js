import { Meteor } from 'meteor/meteor'
import { SyncedCron } from 'meteor/percolate:synced-cron'
import { PendingConfirmation } from '../../Classes/PendingConfirmation'
import { Donor } from '../../Classes/Donor'
import { Beneficiary } from '../../Classes/Beneficiary'

export const AddPendingConfirmationsCron = ({ _id, dueDate, benefTransId, donorTransId }) => {
  if (typeof window === 'undefined') {
    SyncedCron.add({
      name: _id,
      schedule (parser) {
        return parser.recur().on(new Date(dueDate)).fullDate()
      },
      job () {
        const check = PendingConfirmation.findOne(_id)
        if (typeof check !== 'undefined') {
          const benefToBeBanned = Beneficiary.findOne(benefTransId)
          Meteor.users.update(benefToBeBanned.beneficiary, { $set: { isBanned: true } })
          const donor = Donor.findOne(benefTransId)
          donor.beneficiaries = donor.beneficiaries.map((el) => {
            if (el.benefTransId === benefTransId) {
              el.isConfirmed = true
            }
            return el
          })
          donor.save()
          benefToBeBanned.remove()
        }
        SyncedCron.remove(_id)
        return _id
      }
    })
  }
}
