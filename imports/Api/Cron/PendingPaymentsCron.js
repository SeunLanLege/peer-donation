import { Meteor } from 'meteor/meteor'
import { SyncedCron } from 'meteor/percolate:synced-cron'
import { PendingPayment } from '../../Classes/PendingPayment'
import { Donor } from '../../Classes/Donor'
import { sumBy } from 'lodash'
import { Beneficiary } from '../../Classes/Beneficiary'

export const AddPendingPaymentsCron = ({ _id, dueDate, benefTransId, donorTransId }) => {
  if (typeof window === 'undefined') {
    SyncedCron.add({
      name: _id,
      schedule (parser) {
        return parser.recur().on(new Date(dueDate)).fullDate()
      },
      job () {
        const check = PendingPayment.findOne(_id)
        if (typeof check !== 'undefined') {
          const donorToBeBanned = Donor.findOne(donorTransId)
          Meteor.users.update(donorToBeBanned.donor, { $set: { isBanned: true } })
          const beneficiary = Beneficiary.findOne(benefTransId)
          beneficiary.isMatched = false
          beneficiary.isPublished = false
          const amountToBePaid = donorToBeBanned.beneficiaries.filter(el => el.benefTransId === benefTransId)
          beneficiary.amountLeft += sumBy(amountToBePaid, 'amount')
          beneficiary.donors = beneficiary.donors.filter(el => el.donorTransId !== donorTransId)
          beneficiary.save()
          donorToBeBanned.isCompleted = true
          donorToBeBanned.save()
        }
        SyncedCron.remove(_id)
        return _id
      }
    })
  }
}
