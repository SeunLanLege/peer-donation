//  @flow
import { Meteor } from 'meteor/meteor'
import { DonationPackages } from '../../Classes/Packages'
import { Donor } from '../../Classes/Donor'
import { Statistics } from '../../Classes/Statistics'
import { check, Match } from 'meteor/check'

import type { IDonationPackage } from '/imports/type'

Meteor.methods({
  'packages.create' (donationPackage: IDonationPackage) {
    if (!Meteor.userId() && Meteor.user().isKanye) {
      throw new Meteor.Error(403, 'Unauthorized!')
    }

    const pattern = {
      name: String,
      price: Number,
      description: String,
      percentage: Number,
      duration: Number,
      isBonusPackage: Match.Maybe(Boolean),
      parentId: Match.Maybe(String)
    }

    check(donationPackage, pattern)

    const newDonationPackage = new DonationPackages({ ...donationPackage })
    newDonationPackage.save()
  },
  'packages.edit' (id: string, donationPackage: IDonationPackage) {
    if (!Meteor.userId() && Meteor.user().isKanye) {
      throw new Meteor.Error(403, 'Unauthorized!')
    }

    const pattern = {
      name: String,
      _id: String,
      price: Number,
      description: String,
      percentage: Number,
      duration: Number,
      isBonusPackage: Match.Maybe(Boolean),
      parentId: Match.Maybe(String)
    }

    check(donationPackage, pattern)
    check(id, String)
    const dp = DonationPackages.findOne(id)

    if (dp) {
      delete donationPackage._id

      dp.set(donationPackage)
      dp.save()
    }
  },
  'packages.delete' (packageId: string) {
    if (!Meteor.userId() && Meteor.user().isKanye) {
      throw new Meteor.Error(403, 'Unauthorized!')
    }
    check(packageId, String)
    const isActive = Donor.find({ packageId }).fetch()

    if (isActive.length) {
      throw new Meteor.Error(403, 'This Package Has Active Subscribers!')
    }

    const donationPackage = DonationPackages.findOne(packageId)
    if (donationPackage) {
      if (!donationPackage.parentId && !donationPackage.isBonusPackage) {
        DonationPackages.remove({ parentId: packageId })
      }
      donationPackage.remove()
    }
  },
	'stats.get' () {
		return Statistics.findOne()
	},
	'stats.insert' (statistics) {
		const stats = Statistics.findOne()
		if (stats) {
			stats.set(statistics)
			stats.save()
			return;
		}
		const statss = new Statistics({ ...statistics })
		statss.save()
	}
})

Meteor.publish({
  'packages' () {
    return DonationPackages.find()
  },
	'statistics' () {
		return Statistics.find({ }, { createdAt: 1 })
	}
})
