import { Meteor } from 'meteor/meteor'
import { Beneficiary } from '../../Classes/Beneficiary'

Meteor.methods({
  'beneficiary.confirmPayment' ({ _id, donorTransId }) {
    const confirmPayment = Beneficiary.findOne(_id)
    if (!Meteor.userId() && (confirmPayment.beneficiary !== Meteor.userId() || Meteor.user().isKanye)) {
      throw new Meteor.Error(403, 'Unauthorized Action!')
    }
    if (typeof confirmPayment !== 'undefined') {
      confirmPayment.donors = confirmPayment.donors.map((el) => {
        if (el.donorTransId === donorTransId) {
          el.isConfirmed = true
        }
        return el
      })
      confirmPayment.save()
    }
  },
  'beneficiary.togglePublished' (id, status) {
    if (!Meteor.userId() && Meteor.user().isKanye) {
      throw new Meteor.Error(403, 'Unauthorized!')
    }
    check(id, String)
    check(status, Boolean)
    const nb = Beneficiary.findOne(id)
    nb.isPublished = status
    nb.save()
  },
  'beneficiary.new' (userId, packageId) {
    if (!Meteor.userId() && !Meteor.user().isKanye) {
      throw new Meteor.Error(403, 'Unauthorized Action!')
    }
    check(userId, String)
    check(packageId, String)
    const benef = new Beneficiary({ packageId, beneficiary: userId })
    if (benef.package.reward) {
      benef.amountLeft = benef.package.reward
      benef.save()
    }
  }
})

Meteor.publish('user.withdrawals', function () {
  if (!this.userId) {
    throw new Meteor.Error(403, 'Unauthorized!')
  }
  const user = Meteor.users.findOne(this.userId)
  if (user.isKanye) {
    return Beneficiary.find()
  }
  return Beneficiary.find({ beneficiary: this.userId })
})
