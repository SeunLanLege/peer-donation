import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

import Promise from 'bluebird'

export const Method = Promise.promisify(Meteor.call)
export const VerifyEmailToken = Promise.promisify(Accounts.verifyEmail)
export const ResetPassword = Promise.promisify(Accounts.resetPassword)
export const ForgotPasswordReset = Promise.promisify(Accounts.forgotPassword)
export const ChangePasswordMethod = Promise.promisify(Accounts.changePassword)
