import { Meteor } from 'meteor/meteor'
import { Images } from '../../Classes/Images'

Meteor.publish('images.all', function () {
  if (this.userId && Meteor.users.findOne(this.userId).isKanye) {
    return Images.find().cursor
  }
})
