import { Meteor } from 'meteor/meteor'
import { Donor } from '../../Classes/Donor'
import { Beneficiary } from '../../Classes/Beneficiary'
import { check } from 'meteor/check'
import { filter, sumBy } from 'lodash'

/*
  Donor Methods

*/

Meteor.methods({
  'donor.addNew' ({ packageId }) {
    if (!Meteor.userId()) {
      throw new Meteor.Error(403, 'Unauthorized Action!')
    }
    check(packageId, String)
    const newDonor = new Donor({ packageId, donor: Meteor.userId() })
    newDonor.amountLeft = newDonor.package.amount
    newDonor.save()
  },
  'donor.confirmPayment' (data) {
    if (!Meteor.userId()) {
      throw new Meteor.Error(403, 'Unauthorized Action!')
    }
    const { _id, benefTransId, arrayOfImageId } = data
    check(data, { _id: String, benefTransId: String, arrayOfImageId: [String] })

    const confirmPayment = Donor.findOne(_id)
    if (typeof confirmPayment !== 'undefined' && confirmPayment.isPublished) {
      confirmPayment.beneficiaries = confirmPayment.beneficiaries.map((el) => {
        if (el.benefTransId === benefTransId) {
          el.isPaid = true
          el.arrayOfImageId = arrayOfImageId
        }
        return el
      })
      confirmPayment.save()
    }
  },
  'donor.delete' (donorTransId) {
    if (!this.userId && Meteor.user().isKanye) {
      throw new Meteor.Error(403, 'Unauthorized!')
    }
    check(donorTransId, String)
    const donor = Donor.findOne(donorTransId)
    if (donor) {
      donor.beneficiaries.forEach((el) => {
        const benef = Beneficiary.findOne(el.benefTransId)
        const donors = filter(benef.donors, el => el.donorTransId === donorTransId)
        if (donors.length) {
          benef.amountLeft += sumBy(donors, 'amount')
          benef.isMatched = false
          benef.isPublished = false
          benef.donors = benef.donors.filter(el => el.donorTransId !== donorTransId)
          benef.save()
        }
      })
      donor.remove()
    }
  },
  'donor.togglePublished' (id, status) {
    if (!Meteor.userId() && Meteor.user().isKanye) {
      throw new Meteor.Error(403, 'Unauthorized!')
    }
    check(id, String)
    check(status, Boolean)
    const nb = Donor.findOne(id)
    nb.isPublished = status
    nb.save()
  }
})

/*

  Donor Publications

*/

Meteor.publish('user.donations', function () {
  if (!this.userId) {
    throw new Meteor.Error(403, 'Unauthorized!')
  }
  const user = Meteor.users.findOne(this.userId)
  if (user.isKanye) {
    return Donor.find()
  }

  return Donor.find({ donor: this.userId })
})
