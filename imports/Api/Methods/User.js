import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

Accounts.config({
  forbidClientAccountCreation: true
})

Accounts.onCreateUser((options, user) => {
  user.isBanned = false
  user.bankAccName = ''
  user.bankAccNumber = ''
  user.bankName = ''
  user.isKanye = false
  user.bonusPackages = []
  return user
})

Accounts.validateLoginAttempt(
  ({ user }) => {
    if (user && user.isBanned) {
      throw new Meteor.Error(403, 'Your Account has been banned!')
    }
    return true
  }
)

Accounts.urls.enrollAccount = (token) => Meteor.absoluteUrl(`verify-account/${token}`)
Accounts.urls.resetPassword = (token) => Meteor.absoluteUrl(`reset-password/${token}`)

Accounts.emailTemplates.siteName = 'Peer Donation'
Accounts.emailTemplates.from = 'PeerDonation Admin <admin@peerdonation.com>'

Accounts.emailTemplates.enrollAccount.subject = (user) => `Peer Donation: Welcome to Africa's Number One Donation Community!, ${user.emails[0].address}`
Accounts.emailTemplates.enrollAccount.html = (user, url) => {
  const urlWithoutHash = url.replace('#/', '')
  return `Welcome to Peer Donation, 
  Thank you for joining us, we hope that you have a wonderful experience using our platform.
  
  To get started, <a href=${urlWithoutHash}>Click here</a> or 
  
  Direct Link: ${urlWithoutHash}`
}

Accounts.emailTemplates.resetPassword.subject = () => `Peer Donation: Reset Your Password`
Accounts.emailTemplates.resetPassword.html = (user, url) => {
  const urlWithoutHash = url.replace('#/', '')
  return `It appears you forgot your password, we all do. Luckily, you can reset it here ${urlWithoutHash}`
}

Meteor.methods({
  'users.createAccount' ({ email }) {
    try {
      const userId = Accounts.createUser({ email })
      Accounts.sendEnrollmentEmail(userId)
    } catch (e) {
      if (e.reason === 'Email already exists.') {
        const user = Meteor.users.findOne({'emails.address': email})
        if (!user || user.emails[0].verified) {
          throw e
        }
        Accounts.sendEnrollmentEmail(user._id)
      } else {
        throw e
      }
    }
  },
  'users.verifyEmailToken' ({ token }) {
    Accounts.verifyEmail(token)
  },
  'users.updateInfo' (data) {
    check(data, Object)
    if (!Meteor.userId()) {
      throw new Error(403, 'UnAuthorized!')
    }
    Meteor.users.update(Meteor.userId(), {
      $set: { ...data }
    })
  },
  'users.toggleBan' (id) {
    check(id, String)
    if (!Meteor.userId() && Meteor.user().isKanye) {
      throw new Error(403, 'UnAuthorized!')
    }
    const user = Meteor.users.findOne(id)
    Meteor.users.update(id, { $set: { isBanned: !user.isBanned } })
  }
})

Meteor.publish('user', function () {
  if (this.userId) {
    const user = Meteor.users.findOne(this.userId)
    if (user.isKanye) {
      return Meteor.users.find({}, {
        fields: {
          profile: 1,
          fullName: 1,
          phone: 1,
          isBanned: 1,
          isKanye: 1,
          bankAccName: 1,
          emails: 1,
          bankName: 1,
          bonusPackages: 1,
          bankAccNumber: 1 }
      })
    }

    return Meteor.users.find({_id: this.userId}, {
      fields: {
        profile: 1,
        fullName: 1,
        bonusPackages: 1,
        phone: 1,
        isKanye: 1,
        bankAccName: 1,
        bankName: 1,
        bankAccNumber: 1 }
    })
  } else {
    this.ready()
  }
})
