import { observable, action } from 'mobx'
import { Bert } from 'meteor/themeteorchef:bert'

class User {
  @observable data = {
    email: '',
    password: '',
    password2: '',
    fullName: '',
    phone: '',
    bankAccName: '',
    bankName: '',
    isLoading: true,
    bankAccNumber: ''
  }
  @observable userId = ''
  @observable error = ''
  @observable errors = true
  @observable isKanye = false
  @observable currentRoute = {}

	@observable packages = 0
	@observable	donors = 0
	@observable	beneficiaries = 0
	@observable	totalDonations = 0

  @action updateRoute (route) {
    this.currentRoute = route
  }

  @action setIsKanye (status) {
    this.isKanye = status
  }

  @action updateFields (e) {
    this.data[e.target.name] = e.target.value
    if (this.data.email.trim() === '' || this.data.password.trim() === '') {
      this.errors = true
    } else this.errors = false
  }

  @action updateData (key, value) {
    this.data[key] = value
  }

  @action updateUserId (id) {
    this.userId = id
  }

  @action updateError (e) {
    Bert.alert(e.reason, 'danger', 'growl-top-right')
  }

	@action stats (data) {
		if (!data) {
			return;
		}
		Object.keys(data).forEach(el => {
			this[el] = data[el]
		})
	}
}

export default new User()
