import User from './User'
import Misc from './misc'
import DashStore from './DashboardStore'
import Users from './Users'

const store = {
  Misc,
  DashStore,
  Users,
  User
}

export default store
