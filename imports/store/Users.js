//  @flow
import { observable, action, computed } from 'mobx'
import type {IObservableArray} from '/imports/type'

class Users {
  @observable users:IObservableArray = []

  @action setUsers (users: Object[]) {
    this.users = users
  }

  @computed get isBanned (): IObservableArray {
    return this.users.length ? this.users.filter(el => el.isBanned) : []
  }

  @computed get isNotBanned (): IObservableArray {
    return this.users.length ? this.users.filter(el => !el.isBanned) : []
  }
}

export default new Users()
