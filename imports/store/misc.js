import { observable, action } from 'mobx'

class MiscStore {
  @observable isFixed = false

  @action toggleFixed (isfixed) {
    this.isFixed = isfixed
  }
}

export default new MiscStore()
