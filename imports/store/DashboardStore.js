//  @flow
import { observable, action, computed } from 'mobx'
import { map, sum } from 'lodash'

import type { IObservableArray } from '/imports/type'

type prop = 'isDonationLoading' | 'isWithdrawalLoading' | 'isDonationPackageLoading' | 'isImagesLoading'

class DashboardStore {
  @observable donations: IObservableArray = []
  @observable withdrawals: IObservableArray = []
  @observable isDonationLoading: boolean
  @observable isImagesLoading: boolean = false
  @observable isWithdrawalLoading: boolean
  @observable donationPackages: IObservableArray = []
  @observable images: IObservableArray = []

  @action toggleLoading (prop: prop, value: boolean) {
    this[prop] = value
  }

  @action setDonations (donations: Array<Object>) {
    this.donations = donations
  }

  @action setImages (images: []) {
    this.images = images
  }

  @action setWithdrawals (withdrawals: Array<Object>) {
    this.withdrawals = withdrawals
  }

  @action setDonationPackages (donationPackages: Array<Object>) {
    this.donationPackages = donationPackages
  }

  @computed get mainPackages (): IObservableArray {
    return this.donationPackages.filter(el => !el.isBonusPackage)
  }
  @computed get bonusPackages (): IObservableArray {
    return this.donationPackages.filter(el => el.isBonusPackage)
  }

  @computed get totalDonations (): number {
    if (this.donations.length) {
      return sum(map(this.donations, el => {
        if (el.isCompleted) {
          return el.package.amount
        }
        return 0
      }))
    }
    return 0
  }
  @computed get completedDonations (): IObservableArray {
    return this.donations.filter(el => el.isCompleted)
  }

  @computed get completedWithdrawals (): IObservableArray {
    return this.withdrawals.filter(el => el.isCompleted)
  }

  @computed get uncompletedDonations (): IObservableArray | boolean {
    return this.donations.length ? this.donations.filter(el => !el.isCompleted) : []
  }

  @computed get uncompletedWithdrawals (): IObservableArray | boolean {
    return this.withdrawals.length ? this.withdrawals.filter(el => !el.isCompleted) : []
  }

  @computed get totalWithdrawals (): number {
    if (this.withdrawals.length) {
      return sum(map(this.withdrawals, el => {
        if (el.isCompleted) {
          return el.package.reward
        }
        return 0
      }))
    }
    return 0
  }
}

export default new DashboardStore()
