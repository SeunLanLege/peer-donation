import { Mongo } from 'meteor/mongo'
import { Class } from 'meteor/jagi:astronomy'

import { AddPendingConfirmationsCron } from '../Api/Cron/PendingConfirmations'

export const PendingConfirmation = Class.create({
  name: 'PendingConfirmation',
  collection: new Mongo.Collection('pendingConfirmation'),
  fields: {
    userId: {
      type: String
    },
    dueDate: {
      type: String
    },
    benefTransId: {
      type: String
    },
    donorTransId: {
      type: String
    }
  },
  events: {
    afterInsert ({ currentTarget }) {
      AddPendingConfirmationsCron(currentTarget)
    }
  }
})
