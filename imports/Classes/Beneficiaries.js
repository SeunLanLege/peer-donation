import { Meteor } from 'meteor/meteor'
import { Class } from 'meteor/jagi:astronomy'

import { Beneficiary } from './Beneficiary'
import { PendingPayment } from './PendingPayment'
import { User } from './User'

import moment from 'moment'

export const Beneficiaries = Class.create({
  name: 'Beneficiaries',
  fields: {
    benefTransId: {
      type: String
    },
    amount: {
      type: Number
    },
    arrayOfImageId: {
      type: [String],
      default: []
    },
    isPaid: {
      type: Boolean,
      default: false
    },
    isConfirmed: {
      type: Boolean,
      default: false
    },
    beneficiary: {
      type: User,
      optional: true
    }
  },
  behaviors: {
    timestamp: {}
  },
  events: {
    afterInsert ({ currentTarget, target }) {
      if (currentTarget.benefTransId) {
        const benef = Beneficiary.findOne(currentTarget.benefTransId)
        if (typeof benef !== 'undefined') {
          const user = Meteor.users.findOne(benef.beneficiary)
          if (typeof user !== 'undefined') {
            currentTarget.beneficiary = {
              userId: user._id,
              phone: user.phone,
              fullName: user.fullName,
              bankName: user.bankName,
              bankAccNo: user.bankAccNumber,
              bankAccName: user.bankAccName
            }
            target.save()
          }
        }
      }
    },
    afterUpdate ({ currentTarget: currTarget, target }) {
      if (target.isPublished && target.isMatched) {
        if (currTarget.isPaid && !currTarget.isConfirmed) {
          const beneficiary = Beneficiary.findOne(currTarget.benefTransId)
          if (typeof beneficiary !== 'undefined') {
            const rmPendingPayment = PendingPayment.findOne({ userId: target.donor, benefTransId: currTarget.benefTransId, donorTransId: target._id })
            if (rmPendingPayment) {
              rmPendingPayment.remove()
            }
            beneficiary.donors = beneficiary.donors.map((el) => {
              if (target._id === el.donorTransId) {
                el.isPaid = true
                el.paidAt = moment().toISOString()
                el.arrayOfImageId = currTarget.arrayOfImageId
              }
              return el
            })
            beneficiary.save()
          }
        } else if (!currTarget.isPaid) {
          const rmPendingPayment = PendingPayment.findOne({ userId: target.donor, benefTransId: currTarget.benefTransId, donorTransId: target._id })
          if (!rmPendingPayment) {
            const newPendingPayment = new PendingPayment({
              userId: target.donor,
              dueDate: moment().add(1, 'd').toString(),
              benefTransId: currTarget.benefTransId,
              donorTransId: target._id })
            newPendingPayment.save()
          }
        }
      }
      if (currTarget.benefTransId) {
        const benef = Beneficiary.findOne(currTarget.benefTransId)
        if (typeof benef !== 'undefined' && !currTarget.beneficiary) {
          const user = Meteor.users.findOne(benef.beneficiary)
          if (typeof user !== 'undefined') {
            currTarget.beneficiary = {
              userId: user._id,
              phone: user.phone,
              fullName: user.fullName,
              bankName: user.bankName,
              bankAccNo: user.bankAccNumber,
              bankAccName: user.bankAccName
            }
            target.save()
          }
        }
      }
    }
  }
})
