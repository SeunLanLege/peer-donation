import { Class } from 'meteor/jagi:astronomy'

export const User = Class.create({
  name: 'user',
  fields: {
    userId: {
      type: String
    },
    fullName: {
      type: String
    },
    phone: {
      type: String
    },
    bankAccNo: {
      type: String
    },
    bankName: {
      type: String
    },
    bankAccName: {
      type: String
    }
  }
})
