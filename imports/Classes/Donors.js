import { Meteor } from 'meteor/meteor'
import { Class } from 'meteor/jagi:astronomy'

import { PendingConfirmation } from './PendingConfirmation'
import { Donor } from './Donor'
import { User } from './User'

import moment from 'moment'

/*
  TODO : add mailing to beneficiary of payment status
*/

export const Donors = Class.create({
  name: 'donors',
  fields: {
    donorTransId: {
      type: String
    },
    amount: {
      type: Number
    },
    isConfirmed: {
      type: Boolean,
      default: false
    },
    arrayOfImageId: {
      type: [String],
      default: []
    },
    isPaid: {
      type: Boolean,
      default: false
    },
    paidAt: {
      type: String,
      default: '',
      optional: true
    },
    publishedAt: {
      type: String,
      default: '',
      optional: ''
    },
    donor: {
      type: User,
      optional: true
    }
  },
  behaviors: {
    timestamp: {}
  },
  events: {
    afterInsert ({ currentTarget, target }) {
      if (currentTarget.donorTransId) {
        const donor = Donor.findOne(currentTarget.donorTransId)
        if (typeof donor !== 'undefined') {
          const user = Meteor.users.findOne(donor.donor)
          if (typeof user !== 'undefined') {
            currentTarget.donor = {
              userId: user._id,
              phone: user.phone,
              fullName: user.fullName,
              bankName: user.bankName,
              bankAccNo: user.bankAccNumber,
              bankAccName: user.bankAccName
            }
            target.save()
          }
        }
      }
    },
    afterUpdate ({ currentTarget: currTarget, target }) {
      if (target.isPublished && target.isMatched) {
        if (currTarget.isPaid && !currTarget.isConfirmed) {
          /* If is updated as paid, but not confirmed, add a cron from pending payments */
          const rmPaymentConfirmation = PendingConfirmation.findOne({ benefTransId: target._id, donorTransId: currTarget.donorTransId })
          if (!rmPaymentConfirmation) {
            const pendingConfirmation = new PendingConfirmation({
              userId: target.beneficiary,
              donorTransId: currTarget.donorTransId,
              benefTransId: target._id,
              dueDate: moment().add(1, 'd').toString() })
            pendingConfirmation.save()
          }
          if (!currTarget.publishedAt) {
            target.publishedAt = moment().toISOString()
            target.save()
          }
        } else if (currTarget.isPaid && currTarget.isConfirmed) {
          /**
           *  When Beneficiary confirms Payment update
           *  Corressponding donor beneficiary array element
           */
          const donor = Donor.findOne(currTarget.donorTransId)
          if (typeof donor !== 'undefined') {
            const rmPaymentConfirmation = PendingConfirmation.findOne({ benefTransId: target._id, donorTransId: currTarget.donorTransId })
            if (rmPaymentConfirmation) {
              rmPaymentConfirmation.remove()
            }
            donor.beneficiaries.map((el) => {
              if (el.benefTransId === target._id) {
                el.isConfirmed = true
              }
              return el
            })
            donor.save()
          }
        }
      }
      if (currTarget.donorTransId) {
        /*  Set User for UI purposes */
        const donor = Donor.findOne(currTarget.donorTransId)
        if (typeof donor !== 'undefined') {
          const user = Meteor.users.findOne(donor.donor)
          if (typeof user !== 'undefined') {
            currTarget.donor = {
              userId: user._id,
              phone: user.phone,
              fullName: user.fullName,
              bankName: user.bankName,
              bankAccNo: user.bankAccNumber,
              bankAccName: user.bankAccName
            }
            target.save()
          }
        }
      }
    }
  }
})
