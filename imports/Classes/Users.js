import { Class } from 'meteor/jagi:astronomy'
import { Meteor } from 'meteor/meteor'

const BonusPackages = Class.create({
  name: 'bonusPackages',
  fields: {
    amount: {
      type: Number
    },
    packageId: {
      type: String
    },
    donorTransId: {
      type: String
    }
  }
})

const Emails = Class.create({
  name: 'emails',
  fields: {
    address: {
      type: String
    },
    verified: {
      type: Boolean
    }
  }
})

export const Users = Class.create({
  name: 'users',
  collection: Meteor.users,
  fields: {
    fullName: {
      type: String
    },
    services: {
      type: Object,
      optional: true
    },
    emails: {
      type: [Emails],
      optional: true
    },
    phone: {
      type: String,
      optional: true
    },
    isBanned: {
      type: Boolean,
      optional: true
    },
    isKanye: {
      type: Boolean,
      optional: true
    },
    bankAccNo: {
      type: String,
      optional: true
    },
    bonusPackages: {
      type: [BonusPackages],
      optional: true,
      default: []
    },
    bankName: {
      type: String,
      optional: true
    },
    bankAccName: {
      type: String,
      optional: true
    }
  }
})
