import { Mongo } from 'meteor/mongo'
import { Class } from 'meteor/jagi:astronomy'

import { AddPendingPaymentsCron } from '../Api/Cron/PendingPaymentsCron'

export const PendingPayment = Class.create({
  name: 'PendingPayment',
  collection: new Mongo.Collection('pendingPayment'),
  fields: {
    userId: {
      type: String
    },
    dueDate: {
      type: String
    },
    benefTransId: {
      type: String
    },
    donorTransId: {
      type: String
    }
  },
  events: {
    afterInsert ({ currentTarget }) {
      AddPendingPaymentsCron(currentTarget)
    }
  }
})
