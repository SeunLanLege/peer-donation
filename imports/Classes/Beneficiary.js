import { Class } from 'meteor/jagi:astronomy'
import { Meteor } from 'meteor/meteor'
import { find } from 'lodash'

import { Beneficiaries } from '../Collections/Beneficiaries'
import { Donor } from './Donor'
import { Package } from './Package'
import { DonationPackages } from './Packages'
import { Donors } from './Donors'

export const Beneficiary = Class.create({
  name: 'Beneficiary',
  collection: Beneficiaries,
  secured: false,
  fields: {
    donors: {
      type: [Donors],
      default: [] // DonorTransactionId
    },
    beneficiary: {
      type: String // UserId
    },
    amountLeft: {
      type: Number // Initial Package Amount Left
    },
    amount: {
      type: Number,
      transient: true
    },
    package: {
      type: Package,
      transient: true,
      default: {}
    },
    packageId: {
      type: String // Self EXplanatory
    },
    isMatched: {
      type: Boolean,
      default: false
    },
    isPublished: {
      type: Boolean,
      default: false
    },
    isCompleted: {
      type: Boolean,
      default: false
    }
  },
  behaviors: {
    timestamp: {}
  },
  events: {
    afterInit (e) {
      const doc = e.currentTarget
      const donationPackage = DonationPackages.findOne(doc.packageId)
      if (donationPackage && doc.package) {
        const reward = donationPackage.price + (donationPackage.price * donationPackage.percentage)
        doc.amount = reward
        doc.package.amount = donationPackage.price
        doc.package.duration = donationPackage.duration
        doc.package.reward = reward
        doc.package.percentage = donationPackage.percentage
        doc.package.name = donationPackage.name
        doc.package.isBonusPackage = donationPackage.isBonusPackage
      }
    },
    afterInsert ({ currentTarget: target }) {
      const { _id, amountLeft, beneficiary } = target
      /*  Matching Algorithm */
      const donor = Donor.findOne({
        isMatched: false,
        isCompleted: false,
        amountLeft: { $gt: 0 },
        $or: [
          { donor: { $ne: Meteor.userId() } },
          { donor: { $ne: beneficiary } }
        ]},
        { createdAt: 1 })
      if (typeof donor !== 'undefined') {
        if (!find(target.donors, el => el.donorTransId === donor._id) && !find(donor.beneficiaries, el => el.benefTransId === _id)) {
          console.log('Beneficiary afterInsert')
          const result = amountLeft - donor.amountLeft
          let payingAmount
          switch (true) {
            case (result > 0):
              payingAmount = donor.amountLeft
              target.amountLeft = result
              donor.amountLeft = 0
              donor.beneficiaries.push({ amount: payingAmount, benefTransId: _id })
              target.donors.push({ amount: payingAmount, donorTransId: donor._id })
              donor.save()
              target.save()
              break
            case (result === 0):
              payingAmount = amountLeft
              target.amountLeft = 0
              donor.amountLeft = 0
              donor.beneficiaries.push({ amount: payingAmount, benefTransId: _id })
              target.donors.push({ amount: payingAmount, donorTransId: donor._id })
              target.save()
              donor.save()
              break
            case (result < 0):
              payingAmount = amountLeft
              target.amountLeft = 0
              donor.amountLeft = Math.abs(result)
              donor.beneficiaries.push({ amount: payingAmount, benefTransId: _id })
              target.donors.push({ amount: payingAmount, donorTransId: donor._id })
              target.save()
              donor.save()
              break
            default:
              throw new Meteor.Error('No match found in switch')
          }
        }
      }
    },
    afterUpdate (e) {
      const { currentTarget: target } = e
      const { _id, amountLeft, isPublished, beneficiary } = target
      if (isPublished) {
        /* if is Published and all transactions are completed, update as completed */
        let stat = 0
        target.donors.forEach((el) => {
          if (!el.isPaid || !el.isConfirmed) {
            stat += 1
          }
        })
        if (stat === 0) {
          target.isCompleted = true
          target.save()
        }
      }
      if (amountLeft === 0) {
        /* Update as matched for cron job */
        target.isMatched = true
        target.save()
      } else {
        /*  Matching Algorithm */
        const donor = Donor.findOne({
          isMatched: false,
          isCompleted: false,
          amountLeft: { $gt: 0 },
          $or: [
            { donor: { $ne: Meteor.userId() } },
            { donor: { $ne: beneficiary } }
          ]},
          { createdAt: 1 })
        if (typeof donor !== 'undefined') {
          if (!find(target.donors, el => el.donorTransId === donor._id) && !find(donor.beneficiaries, el => el.benefTransId === _id)) {
            console.log('Beneficiary afterUpdate')
            const result = amountLeft - donor.amountLeft
            let payingAmount
            switch (true) {
              case (result > 0):
                payingAmount = donor.amountLeft
                target.amountLeft = result
                donor.amountLeft = 0
                donor.beneficiaries.push({ amount: payingAmount, benefTransId: _id })
                target.donors.push({ amount: payingAmount, donorTransId: donor._id })
                donor.save()
                target.save()
                break
              case (result === 0):
                payingAmount = amountLeft
                target.amountLeft = 0
                donor.amountLeft = 0
                donor.beneficiaries.push({ amount: payingAmount, benefTransId: _id })
                target.donors.push({ amount: payingAmount, donorTransId: donor._id })
                target.save()
                donor.save()
                break
              case (result < 0):
                payingAmount = amountLeft
                target.amountLeft = 0
                donor.amountLeft = Math.abs(result)
                donor.beneficiaries.push({ amount: payingAmount, benefTransId: _id })
                target.donors.push({ amount: payingAmount, donorTransId: donor._id })
                target.save()
                donor.save()
                break
              default:
                throw new Meteor.Error('No match found in switch')
            }
          }
        }
      }
    }
  }
})

if (typeof window !== 'undefined') {
  window.Beneficiary = Beneficiary
  window.DonationPackages = DonationPackages
}
