import { Class } from 'meteor/jagi:astronomy'
import { Meteor } from 'meteor/meteor'

import moment from 'moment'
import { find } from 'lodash'

import { DonationPackages } from './Packages'
import { Donors } from '../Collections/Donors'
import { Package } from './Package'
import { Users } from './Users'
import { Beneficiary } from './Beneficiary'
import { Beneficiaries } from './Beneficiaries'
import { Waiting } from './Waiting'

export const Donor = Class.create({
  name: 'Donor',
  collection: Donors,
  secured: false,
  fields: {
    donor: {
      type: String,
      immutable: true // UserId
    },
    beneficiaries: {
      type: [Beneficiaries],
      default: [] // Empty at first
    },
    packageId: {
      type: String // Self EXplanatory
    },
    amountLeft: {
      type: Number,
      default: 0
    },
    package: {
      type: Package,
      transient: true,
      default: {}
    },
    isMatched: {
      type: Boolean,
      default: false
    },
    isPublished: {
      type: Boolean,
      default: false
    },
    publishedAt: {
      type: String,
      default: '',
      optional: ''
    },
    isCompleted: {
      type: Boolean,
      default: false
    }
  },
  behaviors: {
    timestamp: {}
  },
  events: {
    afterInit (e) {
      const doc = e.currentTarget
      const donationPackage = DonationPackages.findOne(doc.packageId)
      if (donationPackage && doc.package) {
        doc.package.amount = donationPackage.price
        doc.package.duration = donationPackage.duration
        doc.package.reward = donationPackage.reward
        doc.package.name = donationPackage.name
      }
    },
    afterInsert (e) {
      const { currentTarget: target } = e
      const { _id, donor } = target
      const user = Users.findOne(donor)
      if (user.bonusPackages.length) {
        user.bonusPackages = user.bonusPackages.filter(({ amount, packageId }) => {
          const dp = DonationPackages.findOne(packageId)
          if (target.package.amount >= dp.price) {
            const nb = new Beneficiary({ packageId, beneficiary: donor, amountLeft: amount })
            nb.save()
            return false
          }
          return true
        })
        user.save()
      }
      /*  Matching Algorithm */
      const beneficiary = Beneficiary.findOne({
        isMatched: false,
        isCompleted: false,
        amountLeft: { $gt: 0 },
        $or: [
          {beneficiary: { $ne: donor }},
          { beneficiary: { $ne: Meteor.userId() } }
        ] }, { createdAt: 1 })
      if (typeof beneficiary !== 'undefined') {
        if (!find(beneficiary.donors, el => el.donorTransId === _id) && !find(target.beneficiaries, el => el.benefTransId === beneficiary._id)) {
          console.log('Donor afterInsert')
          const result = target.amountLeft - beneficiary.amountLeft
          let payingAmount
          switch (true) {
            case (result > 0):
              payingAmount = beneficiary.amountLeft
              target.amountLeft = result
              beneficiary.amountLeft = 0
              beneficiary.donors.push({ amount: payingAmount, donorTransId: _id })
              target.beneficiaries.push({ amount: payingAmount, benefTransId: beneficiary._id })
              beneficiary.save()
              target.save()
              break
            case (result === 0):
              payingAmount = target.amountLeft
              target.amountLeft = 0
              beneficiary.amountLeft = 0
              beneficiary.donors.push({ amount: payingAmount, donorTransId: _id })
              target.beneficiaries.push({ amount: payingAmount, benefTransId: beneficiary._id })
              target.save()
              beneficiary.save()
              break
            case (result < 0):
              payingAmount = target.amountLeft
              target.amountLeft = 0
              beneficiary.amountLeft = Math.abs(result)
              beneficiary.donors.push({ amount: payingAmount, donorTransId: _id })
              target.beneficiaries.push({ amount: payingAmount, benefTransId: beneficiary._id })
              target.save()
              beneficiary.save()
              break
            default:
              throw new Meteor.Error('No match found in switch')
          }
        }
      }
    },
    afterUpdate ({ currentTarget: target }) {
      const { _id, amountLeft, donor } = target
      if (target.isPublished) {
        if (!target.publishedAt) {
          target.publishedAt = moment().toISOString()
          target.save()
        }
        let stat = 0
        target.beneficiaries.forEach((el) => {
          if (!el.isPaid || !el.isConfirmed) {
            stat += 1
          }
        })
        if (stat === 0) {
          target.isCompleted = true
          const addedWaiting = Waiting.findOne({
            userId: target.donor,
            packageId: target.packageId,
            donorTransId: _id
          })
          if (!addedWaiting) {
            const newBeneficiary = new Waiting({
              userId: target.donor,
              dueDate: moment().add(target.package.duration, 'd').toString(),
              donorTransId: _id,
              packageId: target.packageId,
              package: target.package })
            newBeneficiary.save()
          }
          const dp = DonationPackages.findOne(target.packageId)
          if (dp.parentId) {
            const bonus = DonationPackages.findOne(dp.parentId)
            const amount = bonus.percentage * bonus.price
            const data = { amount, packageId: dp.parentId, donorTransId: _id }
            const user = Users.findOne(target.donor)
            const added = user.bonusPackages.filter(el => el.donorTransId === _id)
            if (!added.length) {
              user.bonusPackages.push(data)
              user.save()
            }
          }
          target.save()
        }
      }
      if (amountLeft === 0) {
        target.isMatched = true
        target.save()
      } else {
        /*  Matching Algorithm */
        const beneficiary = Beneficiary.findOne({
          isMatched: false,
          isCompleted: false,
          amountLeft: { $gt: 0 },
          $or: [
            {beneficiary: { $ne: donor }},
            { beneficiary: { $ne: Meteor.userId() } }
          ] }, { createdAt: 1 })
        if (typeof beneficiary !== 'undefined') {
          if (!find(beneficiary.donors, el => el.donorTransId === _id) && !find(target.beneficiaries, el => el.benefTransId === beneficiary._id)) {
            console.log('Donor afterUpdate')
            const result = target.amountLeft - beneficiary.amountLeft
            let payingAmount
            switch (true) {
              case (result > 0):
                payingAmount = beneficiary.amountLeft
                target.amountLeft = result
                beneficiary.amountLeft = 0
                break
              case (result === 0):
                payingAmount = target.amountLeft
                target.amountLeft = 0
                beneficiary.amountLeft = 0
                break
              case (result < 0):
                payingAmount = target.amountLeft
                target.amountLeft = 0
                beneficiary.amountLeft = Math.abs(result)
                break
              default:
                throw new Meteor.Error('No match found in switch')
            }
            beneficiary.donors.push({ amount: payingAmount, donorTransId: _id })
            target.beneficiaries.push({ amount: payingAmount, benefTransId: beneficiary._id })
            target.save()
            beneficiary.save()
          }
        }
      }
    }
  }
})
