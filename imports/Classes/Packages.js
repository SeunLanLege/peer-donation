import { Class } from 'meteor/jagi:astronomy'
import { Packages } from '../Collections/Packages'

export const DonationPackages = Class.create({
  name: 'packages',
  collection: Packages,
  fields: {
    name: {
      type: String,
      default: ''
    },
    price: {
      type: Number
    },
    description: {
      type: String,
      default: ''
    },
    percentage: {
      type: Number
    },
    reward: {
      transient: true,
      type: Number
    },
    isBonusPackage: {
      type: Boolean,
      default: false,
      optional: true
    },
    duration: {
      type: Number,
      default: 30
    },
    parentId: {
      type: String,
      default: ''
    }
  },
  behaviors: {
    timestamp: {}
  },
  events: {
    afterInit (e) {
      const doc = e.currentTarget
      doc.reward = doc.price + (doc.price * doc.percentage)
    }
  }
})
