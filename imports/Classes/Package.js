import { Class } from 'meteor/jagi:astronomy'

export const Package = Class.create({
  name: 'Pakage',
  fields: {
    amount: {
      type: Number,
      optional: true
    },
    duration: {
      type: Number,
      optional: true
    },
    reward: {
      type: Number,
      optional: true
    },
    name: {
      type: String,
      optional: true
    },
    isBonusPackage: {
      type: String,
      optional: true
    },
    percentage: {
      type: Number,
      optional: true
    }
  }
})
