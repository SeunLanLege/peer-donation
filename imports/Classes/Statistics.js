import { Class } from 'meteor/jagi:astronomy'
import { Mongo } from 'meteor/mongo'

export const Statistics = Class.create({
  name: 'Statistics',
	collection: new Mongo.Collection('statistics'),
  fields: {
    packages: {
      type: Number,
      optional: true
    },
    donors: {
      type: Number,
      optional: true
    },
    beneficiaries: {
      type: Number,
      optional: true
    },
    totalDonations: {
      type: Number,
      optional: true
    }
  }
})
