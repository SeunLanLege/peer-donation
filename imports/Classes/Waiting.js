import { Class } from 'meteor/jagi:astronomy'

import { WaitingCollection } from '../Collections/Waiting'
import { Package } from './Package'
import { newBeneficiary } from '/imports/Api/Cron/newBeneficiary'

export const Waiting = Class.create({
  name: 'Waiting',
  collection: WaitingCollection,
  fields: {
    userId: {
      type: String
    },
    packageId: {
      type: String
    },
    package: {
      type: Package
    },
    dueDate: {
      type: String
    },
    donorTransId: {
      type: String
    }
  },
  events: {
    afterInsert (e) {
      newBeneficiary(e.target)
    }
  }
})
