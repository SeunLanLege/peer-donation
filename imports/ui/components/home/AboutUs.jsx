import React from 'react'
import moment from 'moment'
import CountTo from 'react-count-to'
import { inject, observer } from 'mobx-react'

import { accounting } from 'meteor/iain:accounting'

import { s } from '/imports/ui/style/'

const AboutUs = inject('store')(observer(({ store: { User } }) => (
  <div id='aboutus' style={{ padding: '1rem 0rem' }} className={s('container', 'flex', 'fd-c')}>
		<section className={s('hero-body')}>
      <div className={s('container')}>
        <section className={s('columns')}>
          <div className={s('column', 'is-7')}>
            <div style={{ height: '100%' }} className={s('flex', 'jc-c', 'fd-c', 'ai-c', 'flex-1')}>
              <h1 className={s('title', 'is-2', 'is-spaced')}>ABOUT US</h1>
							<div className={s('underline')} />
              <span className={s('subtitle', 'is-5', 'ln-2')}>
                WE HAVE CREATED A <strong>PLATFORM</strong> THAT HELPS PROVIDE <strong>A SYSTEM OF MONETARY
                EXCHANGE</strong> THAT <strong>BOOSTS</strong> CAPITAL DEVELOPMENT AND INDIVIDUAL <strong>FINANCIAL</strong> GROWTH.
              </span>
            </div>
          </div>
          <div className={s('column', 'lg-logo')}>
            <img className={s('is-fullwidth')} src='/logo.png' alt='Bulma logo' />
          </div>
        </section>
      </div>
    </section>
    <h1 className={s('title', 'is-3', 'has-text-centered')}>Our Stats</h1>
    <nav style={{ margin: '1rem 0rem' }} className={s('level')}>
      <div className={s('level-item', 'has-text-centered')}>
        <div>
          <p className={s('heading')}>Packages</p>
          <CountTo to={User.packages} speed={2000}>{(v) => <p className={s('title')}>{accounting.formatMoney(v, '', 0)}</p> }</CountTo>
          <div className={s('underline')} />
        </div>
      </div>
      <div className={s('level-item', 'has-text-centered')}>
        <div>
          <p className={s('heading')}>Donors</p>
          <CountTo to={User.donors} speed={4500}>{(v) => <p className={s('title')}>{accounting.formatMoney(v, '', 0)}</p> }</CountTo>
          <div className={s('underline')} />
        </div>
      </div>
      <div className={s('level-item', 'has-text-centered')}>
        <div>
          <p className={s('heading')}>Beneficiaries</p>
          <CountTo to={User.beneficiaries} speed={4500}>{(v) => <p className={s('title')}>{accounting.formatMoney(v, '', 0)}</p> }</CountTo>
          <div className={s('underline')} />
        </div>
      </div>
      <div className={s('level-item', 'has-text-centered')}>
        <div>
          <p className={s('heading')}>Total Donations</p>
          <CountTo to={User.totalDonations} speed={4000}>{(v) => <p className={s('title')}>{accounting.formatMoney(v, '₦', 0)}</p> }</CountTo>
          <div className={s('underline')} />
        </div>
      </div>
    </nav>
    <h1 className={s('title', 'is-3', 'has-text-centered')}>Why People Choose Us</h1>
		<div className={s('underline')} />
    <div className={s('columns')}>
      <div className={s('column')}>
        <div className={s('flex', 'fd-c', 'jc-c', 'ai-c')}>
          <h1 className={s('title', 'is-4', 'has-text-centered')}>Security</h1>
          <img className={s('image', 'is-128x128')} src='/security.svg' alt='Secure' />
          <p style={{ margin: '2rem 0rem' }} className={s('subtitle', 'is-6', 'has-text-centered')}>
            We're Secured with https and ssl encryption that prevents your account from being hacked or the website from being DDOS'ed
          </p>
        </div>
      </div>
      <div className={s('column')}>
        <h1 className={s('title', 'is-4', 'has-text-centered')}>24Hour Support</h1>
        <div className={s('flex', 'fd-c', 'jc-c', 'ai-c')}>
          <img className={s('image', 'is-128x128')} src='/delivery.svg' alt='Secure' />
          <p style={{ margin: '2rem 0rem' }} className={s('subtitle', 'is-6', 'has-text-centered')}>
            We're the only peer to peer donation site that Provides 24 hour live chat support!
          </p>
        </div>
      </div>
    </div>
		<div className={s('columns')}>
			<div className={s('column')}>
        <h1 className={s('title', 'is-4', 'has-text-centered')}>SuperFast Payouts</h1>
        <div className={s('flex', 'fd-c', 'jc-c', 'ai-c')}>
          <img style={{ transform: 'scale(3)' }} className={s('image', 'is-128x128')} src='/payout.svg' alt='Secure' />
          <p style={{ margin: '2rem 0rem' }} className={s('subtitle', 'is-6', 'has-text-centered')}>
					Get 100% of your donation in 14 days! 
          </p>
        </div>
      </div>
		</div>
    <footer className={s('flex', 'fd-c', 'jc-c', 'ai-c', 'is-primary', 'footer')}>
      <small style={{ color: '#9a9a9a' }} className={s('has-text-centered')}>© PEER DONATION Global {moment().format('YYYY')}</small>
      <small style={{ color: '#9a9a9a' }} className={s('has-text-centered')}>All Rights Reserved. <a href='/'>peerdonations.com</a> - Privacy Policy.</small>
    </footer>
  </div>
)
)
)

export default AboutUs
