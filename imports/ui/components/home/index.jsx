import React, { PureComponent, PropTypes } from 'react'
import cn from 'classnames/bind'
import { observer, inject } from 'mobx-react'

import ReactRotatingText from 'react-rotating-text'
import AboutUs from './AboutUs'
import { ScrollRenderer } from '../misc/ScrollRenderer'
import { Flow } from './Flow'
// import Testimonials from './Testimonials'
// import Footer from './Footer'
// import { Stars } from './stars'

import style from '../../stylus/index.styl'

const s = cn.bind(style)

@inject('store') @observer
class Body extends PureComponent {

  static propTypes = {
    store: PropTypes.object
  }

  componentDidMount () {
    const hero = document.querySelector('#hero')
    const { Misc } = this.props.store
    window.addEventListener('scroll', () => {
      const targetHeight = hero.offsetTop + hero.scrollHeight
      Misc.toggleFixed(window.scrollY >= targetHeight)
    })
  }
  render () {
    return (
      <main>
        <section
          className={s({
            'is-large': window.innerWidth > 768,
            'is-fullheight': window.innerWidth < 768 },
            'hero', 'sky', 'is-primary', 'is-relative')} id='hero'>
          <div className={s('hero-body', { 'is-fullheight': window.innerWidth < 768 })}>
            <div className={s('container')}>
              <h1 className={s('title', 'is-1')}>
                ACHIEVE FINANCIAL
                <br /> <ReactRotatingText items={['MONDAY.', 'PEACE OF MIND.', 'INDEPENDENCE.']} />
              </h1>
              <h2 className={s('subtitle', 'is-4')}>
              PEER DONATION
              </h2>
              <a href='/register' className={s('button', 'is-primary', 'is-outlined')}>
                Join Us
              </a>
            </div>
          </div>
        </section>
				<Flow />
        <ScrollRenderer>
          <AboutUs />
        </ScrollRenderer>
      </main>
    )
  }
}
export default Body
