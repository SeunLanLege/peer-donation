import React from 'react'
import cn from 'classnames/bind'

import style from '../../stylus/index.styl'

const s = cn.bind(style)

const Testimonials = () => (
  <main className={s('hero', 'is-large')}>
    <section className={s('hero-body')}>
      <main className={s('container', 'ai-c', 'jc-c', 'flex', 'fd-c')}>
        <h1 className={s('title', 'is-1')}>Testimonials</h1>
        <div className={s('tile', 'is-ancestor')}>
          <div className={s('tile', 'is-vertical', 'is-8')}>
            <div className={s('tile')}>
              <div className={s('tile', 'is-parent', 'is-vertical')}>
                <article className={s('tile', 'is-child', 'box')}>
                  <article className={s('media', 'c-w100', 'flex', 'fd-c')}>
                    <div className={s('content', 'flex')}>
                      <figure className={s('media-left')}>
                        <p className={s('image', 'is-64x64')}>
                          <img src='http://bulma.io/images/placeholders/128x128.png' alt='avatar' />
                        </p>
                      </figure>
                      <div className={s('media-content')}>
                        <div className={s('content')}>
                          <div className={s('flex', 'fd-c')}>
                            <strong>Tude Suleiman</strong>
                            <small>Surulere, Lagos</small>
                            <small>31m ago</small>
                            <br />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={s('content')}>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                        eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                    eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                    </div>
                  </article>
                </article>
                <article className={s('tile', 'is-child', 'box')}>
                  <article className={s('media', 'c-w100', 'flex', 'fd-c')}>
                    <div className={s('content', 'flex')}>
                      <figure className={s('media-left')}>
                        <p className={s('image', 'is-64x64')}>
                          <img src='http://bulma.io/images/placeholders/128x128.png' alt='avatar' />
                        </p>
                      </figure>
                      <div className={s('media-content')}>
                        <div className={s('content')}>
                          <div className={s('flex', 'fd-c')}>
                            <strong>Tude Suleiman</strong>
                            <small>Surulere, Lagos</small>
                            <small>31m ago</small>
                            <br />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={s('content')}>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                        eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                    eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                    </div>
                  </article>
                </article>
              </div>
              <div className={s('tile', 'is-parent')}>
                <article className={s('media', 'c-w100', 'flex', 'fd-c', 'box')}>
                  <div className={s('content', 'flex')}>
                    <figure className={s('media-left')}>
                      <p className={s('image', 'is-64x64')}>
                        <img src='http://bulma.io/images/placeholders/128x128.png' alt='avatar' />
                      </p>
                    </figure>
                    <div className={s('media-content')}>
                      <div className={s('content')}>
                        <div className={s('flex', 'fd-c')}>
                          <strong>Tude Suleiman</strong>
                          <small>Surulere, Lagos</small>
                          <small>31m ago</small>
                          <br />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={s('content')}>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                      eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                  eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                  </div>
                </article>
              </div>
            </div>
            <div className={s('tile', 'is-parent')}>
              <article className={s('tile', 'is-child', 'box')}>
                <article className={s('media', 'c-w100', 'flex', 'fd-c')}>
                  <div className={s('content', 'flex')}>
                    <figure className={s('media-left')}>
                      <p className={s('image', 'is-64x64')}>
                        <img src='http://bulma.io/images/placeholders/128x128.png' alt='avatar' />
                      </p>
                    </figure>
                    <div className={s('media-content')}>
                      <div className={s('content')}>
                        <div className={s('flex', 'fd-c')}>
                          <strong>Tude Suleiman</strong>
                          <small>Surulere, Lagos</small>
                          <small>31m ago</small>
                          <br />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={s('content')}>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                      eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                  eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                  </div>
                </article>
              </article>
            </div>
          </div>
          <div className={s('tile', 'is-parent')}>
            <article className={s('tile', 'is-child', 'box')}>
              <article className={s('media', 'c-w100', 'flex', 'fd-c')}>
                <div className={s('content', 'flex')}>
                  <figure className={s('media-left')}>
                    <p className={s('image', 'is-64x64')}>
                      <img src='http://bulma.io/images/placeholders/128x128.png' alt='avatar' />
                    </p>
                  </figure>
                  <div className={s('media-content')}>
                    <div className={s('content')}>
                      <div className={s('flex', 'fd-c')}>
                        <strong>Tude Suleiman</strong>
                        <small>Surulere, Lagos</small>
                        <small>31m ago</small>
                        <br />
                      </div>
                    </div>
                  </div>
                </div>
                <div className={s('content')}>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                    eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare magna eros,
                eu pellentesque tortor vestibulum ut. Maecenas non massa sem. Etiam finibus odio quis feugiat facilisis.</p>
                </div>
              </article>
            </article>
          </div>
        </div>
      </main>
    </section>
  </main>
)

export default Testimonials
