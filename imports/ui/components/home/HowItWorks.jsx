import React from 'react'
import { s } from '/imports/ui/style'

export const HowItWorks = () => (
  <main className={s('container')} style={{padding: '5rem 20px'}}>
    <h1 className={s('title', 'is-1')}>HOW IT WORKS</h1>
    <div className={s('tile is-ancestor')}>
      <div className={s('tile ', 'is-vertical', 'is-8')}>
        <div className={s('tile')}>
          <div className={s('tile', 'is-parent', 'is-vertical')}>
            <div className={s('card')}>
              <div data-background-color='green' className={s('card-header')}>
                <h1 className={s('title', 'is-3')}>Packages</h1>
              </div>
              <div className={s('card-content')}>
                <p className={s('subtitle', 'is-6')}>We have 4 packages for now, Basic, Advanved, Gold and Premium</p>
                <p className={s('subtitle', 'is-6')}>All Packages Provide 150% returns on donation with extra 50% bonus on re-donation of equivalent package or higher</p>
                <p className={s('subtitle', 'is-6')}>All except Basic is eligible for the 50% bonus</p>
              </div>
            </div>
            <div className={s('card')}>
              <div data-background-color='purple' className={s('card-header')}>
                <h1 className={s('title', 'is-3')}>Withdrawals</h1>
              </div>
              <div className={s('card-content')}>
                <p className={s('subtitle', 'is-6')}>
                  After a 14 days, You'll be automatically converted to a beneficiary and you'll be paid by other members.
                </p>
              </div>
            </div>
          </div>
          <div className={s('tile', 'is-parent')}>
            <div className={s('card')}>
              <div data-background-color='blue' className={s('card-header')}>
                <h1 className={s('title', 'is-3')}>Donations</h1>
              </div>
              <div className={s('card-content')}>
                <p className={s('subtitle', 'is-6')}>After Purchasing a package, You will be paired with beneficiaries.(ie The people you'll pay to)</p>
                <p className={s('subtitle', 'is-6')}>Pairings happen everyday at 5:00am, so if you purchase a package check back the next day to see your beneficiaries.</p>
                <p className={s('subtitle', 'is-6')}>After You've Paid and you've confirmed, your donations. You'll be awarded a bonus package (depending on the package purchased) that will be automatically dispensed upon resubscription to another package</p>
                <br />
                <p className={s('title', 'is-5', 'text-danger')}>WARNING! YOUR ACCOUNT WILL BE BANNED IF</p>
                <p className={s('subtitle', 'is-6', 'text-danger')}>
                  - You fail to pay and confirm your beneficiaries after 24 hours of pairing
                </p>
                <p className={s('subtitle', 'is-6', 'text-danger')}> - You upload Fake proof of payment</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={s('tile', 'is-parent')}>
        <div className={s('card')}>
          <div className={s('card-header')}>
            <h1 className={s('title', 'is-3')}>Terms And Conditions</h1>                
          </div>
          <div className={s('card-content')}>
            <p style={{ fontSize: '9px' }} className={s('subtitle', 'is-6', 'text-info')}>By Signing up, you implicitly agree to our <a href='/terms.txt'>terms and conditions</a></p>
          </div>
        </div>
      </div>
    </div>
  </main>
)
