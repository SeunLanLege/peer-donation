import React from 'react'
import { s } from '/imports/ui/style'


export const Flow = () => (
	<div style={{ padding: '30px 0px' }} className={s('hero', 'flex', 'jc-c', 'ai-c')}>
		<h1 className={s('title', 'is-3', 'has-text-centered', 'is-marginless')}>Donation Process</h1>
		<div className={s('underline')} />
		<ul style={{ width: window.innerWidth > 480 ? '70%' : 'auto', marginTop: 40}} className={s('progress-indicator', {'stacked': window.innerWidth <= 480}, 'nocenter')}>
			<li className={s('completed')}>
				<span className={s('bubble')} />
				<span className={s({'stacked-text': window.innerWidth <= 480 }, 'subdued', 'flex', 'fd-c')}>
					<span>Step 1.</span>
					<span> Register </span>
				</span>
				<i style={{ fontSize: 44 }} className='material-icons'>assignment</i>
			</li>
			<li className={s('completed',)}>
				<span className={s('bubble')} />
				<span className={s({'stacked-text': window.innerWidth <= 480 }, 'subdued', 'flex', 'fd-c')}>
					<span> Step 2.</span>
					<span> Choose a plan </span>
				</span>
				<i style={{ fontSize: 44 }} className='material-icons'>list</i>
			</li>
			<li className={s('completed')}>
				<span className={s('bubble')} />
				<span className={s({'stacked-text': window.innerWidth <= 480 }, 'subdued',  'flex', 'fd-c')}>
					<span> Step 3.</span>
					<span>Donate to a member</span>
				</span>
				<i style={{ fontSize: 44 }} className='material-icons'>monetization_on</i>
			</li>
			<li className={s('completed')}>
				<span className={s('bubble')} />
				<span className={s({'stacked-text': window.innerWidth <= 480 }, 'subdued', 'flex', 'fd-c')}>
					<span> Step 4. </span>
					<span>Get paid 100%</span>
				</span>
				<i style={{ fontSize: 44 }} className='material-icons'>attach_money</i>
			</li>
			<li className={s('completed')}>
				<span className={s('bubble')} />
				<span className={s({'stacked-text': window.innerWidth <= 480 }, 'subdued', 'flex', 'fd-c')}>
					<span> Step 5.</span>
					<span>Recycle</span>
				</span>
				<i style={{ fontSize: 44 }} className='material-icons'>refresh</i>
			</li>
		</ul>
	</div>
)
