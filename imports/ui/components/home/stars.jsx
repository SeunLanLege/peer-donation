import React from 'react'
import { s } from '/imports/ui/style'

export const Stars = () => (
  <div className={s('sky')}>
    <div className={s('stars')} />
    <div className={s('stars1')} />
    <div className={s('stars2')} />
    <div className={s('shooting-stars')} />
  </div>
  )

