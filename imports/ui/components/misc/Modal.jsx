import React, { PropTypes } from 'react'
import cn from 'classnames/bind'
import styl from '/imports/ui/stylus/index.styl'

const s = cn.bind(styl)

const Modal = ({ isActive, children }) => (
  <div className={s('modal', { 'is-active': isActive })}>
    <div className={s('modal-background')} />
    <div className={s('modal-content')}>
      {children}
    </div>
    <button className={s('modal-close')} />
  </div>
)

Modal.propTypes = {
  children: PropTypes.element.isRequired,
  isActive: PropTypes.bool.isRequired
}

export default Modal
