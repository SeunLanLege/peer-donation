import React, { PureComponent, PropTypes } from 'react'

export class ScrollRenderer extends PureComponent {
  static propTypes = {
    children: PropTypes.element.isRequired
  }

  state = {
    id: `idhjjh`,
    show: false
  }

  componentDidMount () {
    const el = document.querySelector(`#${this.state.id}`)
    const elTop = el.scrollTop
    const elBottom = elTop + el.scrollHeight
    const screenBottom = window.pageYOffset + window.innerHeight
    const screenTop = window.pageYOffset

    if ((screenBottom > elTop) && (screenTop < elBottom)) {
      this.setState((prev) => ({ ...prev, show: true }))
    }
    window.addEventListener('scroll', () => {
      const el = document.querySelector(`#${this.state.id}`)
      const elTop = el.scrollTop
      const elBottom = elTop + el.scrollHeight
      const screenBottom = window.pageYOffset + window.innerHeight
      const screenTop = window.pageYOffset

      if ((screenBottom > elTop) && (screenTop < elBottom)) {
        this.setState((prev) => ({ ...prev, show: true }))
      }
    })
  }

  render () {
    const { children } = this.props
    return (
      <main style={{ height: 200 }} id={this.state.id}>
        {this.state.show && children}
      </main>
    )
  }
}
