import React, { PureComponent, PropTypes } from 'react'
import { observer, inject } from 'mobx-react'
import classNames from 'classnames/bind'
import isEqual from 'lodash/isEqual'
import { FlowRouter } from 'meteor/kadira:flow-router'
import styl from '../../../stylus/index.styl'
import { ResetPassword } from '../../../../Api/Methods/Methods'
import { observable, action } from 'mobx'
import { Stars } from '/imports/ui/components/home/stars'

import { Bert } from 'meteor/themeteorchef:bert'

const s = classNames.bind(styl)

@inject('store') @observer
export default class ResetPasswordUI extends PureComponent {
  static propTypes = {
    store: PropTypes.object
  }

  @observable isLoading = false

  @action toggleLoading (status) {
    this.isLoading = status
  }

  verifyToken = async () => {
    const { token, store: { User } } = this.props
    const { data: { password } } = User
    try {
      await ResetPassword(token, password)
      Bert.alert('Password reset Successfully!', 'success', 'growl-top-right')
      FlowRouter.go('/account/dashboard')
    } catch (e) {
      User.updateError(e)
    } finally {
      this.toggleLoading(false)
    }
  }

  change = (e) => {
    const { User } = this.props.store
    User.updateFields(e)
  }

  submit = (e) => {
    this.toggleLoading(true)
    e.preventDefault()
    const { store } = this.props
    const { password, password2 } = store.User.data
    if (isEqual(password, password2)) {
      this.verifyToken()
    }
  }

  render () {
    const { User: { data: { password, password2 } } } = this.props.store
    return (
      <main className={s('flex', 'fd-c', 'w-100', 'jc-c', 'ai-c', 'h-100', 'columns', 'translucent-logo')}>
        <Stars />
        <div className={s('column', 'is-11-mobile', 'is-3-tablet')}>
          <div className={s('card')}>
            <div data-background-color='purple' className={s('card-header')}>
              <h1 className={s('title', 'is-4')}>Reset Password</h1>
            </div>
            <div className={s('card-content')}>
              <form className={s('flex', 'fd-c')} onSubmit={this.submit}>
                <label className={s('label')} htmlFor='password'>New Password</label>
                <input
                  autoComplete='off'
                  className={s('input')}
                  type='password'
                  name='password'
                  value={password}
                  onChange={this.change} />
                <label className={s('label')} htmlFor='password2'>Verify Password</label>
                <input
                  autoComplete='off'
                  className={s('input')}
                  type='password'
                  name='password2'
                  value={password2}
                  onChange={this.change} />
                <br />
                <button
                  disabled={!((password && password2) && (password === password2))}
                  type='submit'
                  className={s('button', 'is-primary', { 'is-loading': this.isLoading })}>Submit</button>
              </form>
            </div>
          </div>
        </div>
      </main>
    )
  }
}
