//  @flow
import React from 'react'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Bert } from 'meteor/themeteorchef:bert'
import autorun from 'meteor/space:tracker-mobx-autorun'

import DashAutorun from '../../../../autoruns/DashboardAutorun'

type propss = { className:string, children: any };

const LogoutButton = (props: propss) => {
  const {className, children} = props
  const logout = (e) => {
    e.preventDefault()
    try {
      Meteor.logout()
      autorun(DashAutorun).stop()
      Bert.alert('SuccessFully Logged Out', 'success', 'growl-top-right')
    } catch (e) {
      Bert.alert(e.message, 'success', 'growl-top-right')
    } finally {
      FlowRouter.go('/')
    }
  }
  return (
    <a className={className} onClick={logout}>{children}</a>
  )
}

export default LogoutButton
