import React, { PureComponent, PropTypes } from 'react'
import { Meteor } from 'meteor/meteor'
import { observer, inject } from 'mobx-react'
import { observable, action } from 'mobx'
import { FlowRouter } from 'meteor/kadira:flow-router'
import Promise from 'bluebird'
import cn from 'classnames/bind'
import { Bert } from 'meteor/themeteorchef:bert'
import { Stars } from '/imports/ui/components/home/stars'

import styl from '../../../stylus/index.styl'

const s = cn.bind(styl)

const meteorLogin = Promise.promisify(Meteor.loginWithPassword)

@inject('store') @observer
export default class Login extends PureComponent {
  static propTypes = {
    store: PropTypes.object.isRequired
  }

  @observable isLoading = false
  @action toggleLoading (status) {
    this.isLoading = status
  }

  login = async (e) => {
    e.preventDefault()
    this.toggleLoading(true)
    const { User } = this.props.store
    const { email, password } = User.data
    try {
      await meteorLogin(email, password)
      Meteor.user().isKanye ? FlowRouter.go('/blkkskknhdd/dashboard') : FlowRouter.go('/account/dashboard')
      Bert.alert('Logged In Successfully!', 'success', 'growl-top-right')
    } catch (e) {
      User.updateError(e)
    } finally {
      this.toggleLoading(false)
    }
  }

  handleChange = (e) => {
    const { User } = this.props.store
    User.updateFields(e)
  }

  render () {
    const { data: { email, password }, errors } = this.props.store.User
		const ele = document.querySelector('#box')
    return (
      <main id='#box' style={{ marginTop: ele && ele.offsetTop }} className={s('w-100', 'flex', 'jc-c', 'ai-c', 'h-100', 'columns', 'is-paddingless', 'is-marginless', 'translucent-logo')}>
        <Stars />
        <div className={s('column', 'is-11-mobile', 'is-3-tablet')}>
          <div className={s('card')}>
            <div data-background-color='purple' className={s('card-header')}>
              <h1 className={s('title', 'is-4')}>Login</h1>
            </div>
            <div className={s('card-content')}>
              <form autoComplete='off' onSubmit={this.login} className={s('box')}>
                <label htmlFor='email'className={s('label')}>Email Address</label>
                <input
                  autoComplete='off'
                  className={s('input')}
                  value={email}
                  onChange={this.handleChange}
                  type='email'
                  name='email'
                />
                <label htmlFor='password'className={s('label')}>Password</label>
                <input
                  autoComplete='off'
                  className={s('input')}
                  value={password}
                  onChange={this.handleChange}
                  type='password'
                  name='password'
                />
                <br /><br />
                <button
                  className={s('button', 'is-fullwidth', 'is-success', { 'is-disabled': (errors || this.isLoading || !email || !password), 'is-loading': this.isLoading })}
                  disabled={errors || this.isLoading || !email || !password}
                  onClick={this.login}>Login</button><br />
                <a href='/forgot-password'>Forgot Password?</a>
                <br />
                <a href='/register' className={s('button', 'is-primary', 'is-fullwidth')}>
                  Register
                </a>
              </form>
            </div>
          </div>
        </div>
      </main>
    )
  }
}
