import React, { PureComponent, PropTypes } from 'react'
import { observer, inject } from 'mobx-react'
import { observable, action } from 'mobx'
import cn from 'classnames/bind'
import { Stars } from '/imports/ui/components/home/stars'

import { ForgotPasswordReset } from '../../../../Api/Methods/Methods'
import styl from '../../../stylus/index.styl'

const s = cn.bind(styl)

@inject('store') @observer
class ForgotPassword extends PureComponent {
  @observable isLoading = false
  @observable isDone = false

  static propTypes = {
    store: PropTypes.object
  }

  @action toggleLoading (status) {
    this.isLoading = status
  }

  @action setDone (status) {
    this.isDone = status
  }

  submit = async (e) => {
    this.toggleLoading(true)
    e.preventDefault()
    const { User } = this.props.store
    const { email } = User.data
    try {
      await ForgotPasswordReset({ email })
      this.setDone(true)
    } catch (e) {
      User.updateError(e)
    } finally {
      this.toggleLoading(false)
    }
  }

  rndrStatus () {
    const { User } = this.props.store
    const { email } = User.data
    if (this.isDone) {
      return (
        <div className={s('card')}>
          <div data-background-color='green' className={s('card-header')}>
            <h1 className={s('title', 'is-4')}>Reset Link Sent!</h1>
          </div>
          <div className={s('card-content')}>
            <p>We've sent a password reset link to {email}, please visit it in order to reset your password</p>
          </div>
        </div>
      )
    }
    return (
      <div className={s('card')}>
        <div data-background-color='purple' className={s('card-header')}>
          <h1 className={s('title', 'is-4')}>Forgot Password?</h1>
        </div>
        <div className={s('card-content')}>
          <form onSubmit={this.submit} className={s('flex', 'fd-c')}>
            <label className={s('label')} htmlFor='email'>Email Address</label>
            <input
              type='email'
              name='email'
              value={email}
              autoComplete='off'
              className={s('input')}
              onChange={e => User.updateFields(e)} />
            <br />
            <button
              disabled={this.isLoading || !email}
              type='submit'
              className={s('button', 'is-primary', 'is-fullwidth', { 'is-loading': this.isLoading })}>Register</button>
          </form>
        </div>
      </div>
    )
  }

  render () {
    return (
      <main className={s('h-100', 'flex', 'fd-c', 'jc-c', 'ai-c', 'columns', 'translucent-logo')}>
        <Stars />
        <div className={s('column', 'is-11-mobile', 'is-3-tablet')}>
          {this.rndrStatus()}
        </div>
      </main>
    )
  }
}

export default ForgotPassword
