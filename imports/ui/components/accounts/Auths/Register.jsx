import React, { PureComponent, PropTypes } from 'react'
import { observer, inject } from 'mobx-react'
import { observable, action } from 'mobx'
import cn from 'classnames/bind'
import { Stars } from '/imports/ui/components/home/stars'

import styl from '../../../stylus/index.styl'
import { Method } from '../../../../Api/Methods/Methods'

const s = cn.bind(styl)

@inject('store') @observer
class Register extends PureComponent {
  static propTypes = {
    store: PropTypes.object
  }

  @observable isLoading = false
  @observable isDone = false

  @action toggleLoading (status) {
    this.isLoading = status
  }

  @action setDone (status) {
    this.isDone = status
  }

  register = async (e) => {
    e.preventDefault()
    this.toggleLoading(true)
    const { User } = this.props.store
    const { data: { email } } = User
    try {
      await Method('users.createAccount', { email })
      this.setDone(true)
    } catch (e) {
      User.updateError(e)
      this.setDone(false)
    } finally {
      this.toggleLoading(false)
    }
  }

  renderRegister = () => {
    const { User } = this.props.store
    const { data: { email } } = User
    if (this.isDone) {
      return (
        <div className={s('flex', 'fd-c', 'container', 'column', 'is-3-tablet', 'is-11-mobile')}>
          <div className={s('card')}>
            <div data-background-color='green' className={s('card-header')}>
              <h1 className={s('title', 'is-4')}>Email Sent!</h1>
            </div>
            <div className={s('card-content')}>
              <p>We've sent a verification link to {email}, please visit it in order to complete the registeration process</p>
            </div>
          </div>
        </div>
      )
    }
    return (
      <div className={s('column', 'is-11-mobile', 'is-3-tablet')}>
        <div className={s('card')}>
          <div data-background-color='purple' className={s('card-header')}>
            <h1 className={s('title', 'is-4')}>SignUp</h1>
          </div>
          <div className={s('card-content')}>
            <form onSubmit={this.register} className={s('flex', 'fd-c', 'column')}>
              <label htmlFor='email' className={s('label')}>Email Address</label>
              <input
                type='email'
                name='email'
                value={email}
                autoComplete='off'
                className={s('input')}
                onChange={e => User.updateFields(e)} />
              <br />
              <button
                disabled={this.isLoading || !email}
                type='submit'
                className={s('button', 'is-primary', { 'is-loading': this.isLoading })}>Register</button>
            </form>
            <p className={s('subtitle', 'is-6', 'has-text-centered')}>You'll be sent a link to verify your email address and set your password.</p>
          </div>
        </div>
      </div>
    )
  }

  render () {
    return (
      <main className={s('w-100', 'flex', 'jc-c', 'ai-c', 'h-100', 'fd-c', 'columns', 'is-marginless', 'translucent-logo')}>
        <Stars />
        {this.renderRegister()}
      </main>
    )
  }
}

export default Register
