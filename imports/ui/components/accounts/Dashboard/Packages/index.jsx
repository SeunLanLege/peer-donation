//  @flow
import React, { PureComponent } from 'react'
import { observer, inject } from 'mobx-react'
import { Bert } from 'meteor/themeteorchef:bert'
import { chunk } from 'lodash'
import { accounting } from 'meteor/iain:accounting'
import { FlowRouter } from 'meteor/kadira:flow-router'

import { observable, action } from 'mobx'

import { Method } from '../../../../../Api/Methods/Methods'
import { s } from '../../../../style'

type IPackage = {
  name: string,
  price: number,
  _id: string,
  description: string,
  duration: string,
  percentage: number
}

type Props = { store: Object }

@inject('store')
@observer
export default class SelectPackage extends PureComponent {
  props: Props

  @observable id = ''
  @action setId = (id: string) => {
    this.id = id
  }

  purchase = async (packageId: string) => {
    this.setId(packageId)
    try {
      await Method('donor.addNew', { packageId })
      FlowRouter.go('/account/dashboard')
      Bert.alert('Purchased package successfully!', 'success', 'growl-top-right')
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setId('')
    }
  }

  rndrPackageItems = (donationPackage: IPackage) => {
    const { name, price, _id, description, percentage, duration } = donationPackage
    return (
      <div key={_id} className={s('card', 'column', 'is-4-tablet')}>
        <div data-background-color='green' className={s('card-header', 'flex', 'jc-sb', 'ai-c')}>
          <h1 className={s('title', 'is-4', 'is-paddingless', 'is-marginless')}>{name.toUpperCase()}</h1>
          <h1 className={s('title', 'is-4')}>{accounting.formatMoney(price, '₦', 0)}</h1>
        </div>
        <div className={s('card-content')}>
          {description}
          <div className={s('flex', 'jc-sb', 'fd-c')}>
            <div className={s('flex', 'jc-sb')}>
              <p className={s('subtitle', 'is-6')}>Percentage</p>
              <p className={s('subtitle', 'is-6')}>{percentage * 100}%</p>
            </div>
            <div className={s('flex', 'jc-sb')}>
              <p className={s('subtitle', 'is-6')}>Duration</p>
              <p className={s('subtitle', 'is-6')}>{duration} days</p>
            </div>
          </div>
          <button
            disabled={this.id === _id}
            className={s('is-success', 'is-fullwidth', 'button', { 'is-loading': this.id === _id })}
            onClick={() => this.purchase(_id)}>
            Purchase</button>
        </div>
        <div className={s('card-footer')}>
          <small> Eligible for {accounting.formatMoney((price * percentage) + price, '₦', 0)} withdrawal after {duration} days</small>
        </div>
      </div>
    )
  }

  rndrPackages = () => {
    const { DashStore } = this.props.store
    const { mainPackages } = DashStore
    return !DashStore.isDonationPackageLoading &&
      chunk(mainPackages, 3)
        .map((el, i) =>
          <section key={i} className={s('columns', 'is-marginless')}>
            {el.map(this.rndrPackageItems)}
          </section>
        )
  }

  render () {
    return (
      <main>
        <h1 className={s('subtitle', 'is-5')}>Packages</h1>
        {this.rndrPackages()}
      </main>
    )
  }
}
