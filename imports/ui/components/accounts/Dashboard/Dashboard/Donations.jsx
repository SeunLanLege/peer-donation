//  @flow
import React, { PureComponent } from 'react'
import { inject, observer } from 'mobx-react'
import { observable, action } from 'mobx'
import moment from 'moment'
import { findIndex } from 'lodash'
import DropZone from 'react-dropzone'

import { Meteor } from 'meteor/meteor'
import { accounting } from 'meteor/iain:accounting'
import { Bert } from 'meteor/themeteorchef:bert'

import { Method } from '../../../../../Api/Methods/Methods'
import { Images } from '../../../../../Classes/Images'
import { s } from '../../../../style'

import type { DonationProps, benefInterface, IObservableArray } from '/imports/type'

type Progress = {
  value: number,
  name: string
}

declare interface ImageFile extends File {
  preview: string
}

@inject('store')
@observer
export class Donations extends PureComponent {
  props: DonationProps

  @observable isModalOpen: boolean = false
  @observable progress: IObservableArray<Progress> = []
  @observable currentDonation: ?benefInterface
  @observable images: IObservableArray<ImageFile> = []
  @observable isUploading: boolean = false
  @observable isLoading: boolean = false
  @observable timeLeft: string = ''
  interval: number

  @action showModal = (status: boolean): void => {
    this.isModalOpen = status
  }

  @action toggleIsLoading = (status: boolean) => {
    this.isLoading = status
  }

  @action toggleIsUploading = (status: boolean) => {
    this.isUploading = status
  }

  @action setProgress = (progress: ?Progress) => {
    if (progress) {
      const { name, value } = progress
      const index = findIndex(this.progress, el => name === el.name)
      if (index === -1) {
        this.progress.push(progress)
      } else {
        this.progress[index].value = value
      }
    } else this.progress.replace([])
  }

  @action setCurrentDonation = (donor: ?benefInterface): void => {
    if (!donor) {
      this.currentDonation = null
    } else this.currentDonation = donor
  }

  @action setImages = (images: ?Array<ImageFile>): void => {
    if (images) {
      this.images.push(...images)
    } else this.images.replace([])
  }

  @action rmImage = (index: number, e: Event):void => {
    e.stopPropagation()
    if (this.images.length) {
      this.images.splice(index, 1)
    }
  }

  rndrImagePreview = (image: ImageFile, index: number) => (
    <div key={image.lastModifiedDate} style={{ paddingRight: 10 }} className={s('pos-rel')}>
      <button onClick={(e) => this.rmImage(index, e)} className={s('delete', 'top-right')} />
      <img className={s('is-128x128', 'image')} src={image.preview} alt={image.name} />
    </div>
  )

  rndrProgress = (status: Progress, i: number) => {
    const { name, value } = status
    return (
      <div className={s('is-fullwidth')} key={i}>
        <p>Uploading {name} {value}%</p>
        <progress
          className={s('progress', 'is-primary', 'is-small', 'is-fullwidth')}
          value={value}
          max={100}>
          {value}%
        </progress>
      </div>
    )
  }

  uploadImages = (): Array<Promise<string>> => {
    const imageUpload = this.images.map((file) => {
      return Images.insert({
        file,
        streams: 'dynamic',
        chunkSize: 'dynamic'
      }, false)
    })
    return imageUpload.map(file => {
      file.on('start', () => {
        this.toggleIsUploading(true)
      })

      file.on('progress', (progress: number, fileObj: File) => {
        this.setProgress({ value: progress, name: fileObj.name })
      })

      const id = new Promise((resolve) => {
        file.on('end', async (err, fileObj) => {
          if (err) {
            Bert.alert(err.reason, 'danger', 'growl-top-right')
          } else {
            resolve(fileObj._id.toString())
          }
        })
      })
      file.start()
      return id
    })
  }

  confirmPayment = async (benefTransId: string) => {
    const { _id } = this.props
    this.showModal(true)
    try {
      if (!this.images.length) {
        throw new Meteor.Error(403, 'Image is required!')
      }
      const arrayOfImageId = await Promise.all(this.uploadImages())
      this.toggleIsLoading(true)
      this.toggleIsUploading(false)
      await Method('donor.confirmPayment', { benefTransId, _id, arrayOfImageId })
      this.setImages(null)
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.toggleIsLoading(false)
      this.showModal(false)
    }
  }

  rndrModal = () => {
    if (this.currentDonation) {
      const { beneficiary, benefTransId, amount } = this.currentDonation
      return (
        <div className={s('modal', { 'is-active': this.isModalOpen })}>
          <div onClick={() => this.showModal(false)} className={s('modal-background')} />
          <div className={s('modal-card')}>
            <header className={s('modal-card-head')}>
              <p className={s('modal-card-title')}>Confirm Payment</p>
              <button onClick={() => this.showModal(false)} className={s('delete')} />
            </header>
            <section className={s('modal-card-body')}>
              <div className={s('flex', 'jc-sb')}>
                <h2 className={s('title', 'is-5')}>Name</h2><h1 className={s('title', 'is-4')}>{beneficiary.fullName}</h1>
              </div>
              <div className={s('flex', 'jc-sb')}>
                <h2 className={s('title', 'is-5')}>Amount</h2> <h2 className={s('title', 'is-4')}> {accounting.formatMoney(amount, '₦')}</h2>
              </div>
              <div>
                <h2 className={s('title', 'is-5', 'is-bold')}>Bank Details</h2>
                <div className={s('flex', 'jc-sb')}>
                  <h2 className={s('title', 'is-5')}>Bank Name</h2><h2 className={s('title', 'is-4')}>{beneficiary.bankName}</h2>
                </div>
                <div className={s('flex', 'jc-sb')}>
                  <h2 className={s('title', 'is-5')}>Bank Account Number</h2><h2 className={s('title', 'is-4')}>{beneficiary.bankAccNo}</h2>
                </div>
                <div className={s('flex', 'jc-sb')}>
                  <h2 className={s('title', 'is-5')}>Bank Name</h2><h2 className={s('title', 'is-4')}>{beneficiary.bankAccName}</h2>
                </div>
              </div>
              <DropZone
                className={s('is-fullwidth', 'flex', 'fw-w', 'fd-r', 'dropZone', { 'jc-c': !this.images.length }, 'ai-c')}
                activeClassName={s('is-success')}
                rejectClassName={('is-danger')}
                accept='image/*'
                onDrop={this.setImages}
              >
                {this.images.length
                  ? this.images.map(this.rndrImagePreview)
                  : <div>Drag images here or click to select images to upload</div>}
                {!!this.images.length && <p>Click to add more images</p>}
              </DropZone>
            </section>
            <footer className={s('modal-card-foot', 'flex', 'fd-c', 'ai-s')}>
              <button
                disabled={!this.images.length || this.isUploading}
                onClick={() => this.confirmPayment(benefTransId)}
                className={s('button', 'is-success', { 'is-loading': this.isUploading || this.isLoading })}
              >Confirm</button>
              <div className={s('flex', 'fd-c')}>
                {this.progress && this.progress.map(this.rndrProgress)}
              </div>
            </footer>
          </div>
        </div>
      )
    }
  }

  showModalContent = (benefs: benefInterface): void => {
    this.setCurrentDonation(benefs)
    this.showModal(true)
  }

  componentDidMount () {
    const { publishedAt } = this.props
    if (publishedAt) {
      const eventTime = moment(publishedAt).add(1, 'd').unix()
      const diffTime = eventTime - moment().unix()
      const interval = 1000
      let newTime = moment.duration(diffTime * 1000, 'milliseconds').asMilliseconds()
      this.interval = setInterval(() => {
        const time = moment.duration(newTime -= interval, 'milliseconds')
        this.timeLeft = `${time.days()} days ${time.hours()} hrs ${time.minutes()} mins ${time.seconds()} s`
      }, interval)
    }
  }

  componentWillUnMount () {
    clearInterval(this.interval)
  }

  rndrBeneficiaries = (benefs: benefInterface) => {
    const { isPaid, isConfirmed, beneficiary: user, amount } = benefs
    return (
      <div key={benefs.benefTransId} style={{ margin: '30px 0px' }} className={s('flex', 'jc-sb', 'ai-c')}>
        <div style={{ height: 80 }} className={s('flex', 'fd-c', 'jc-sb')}>
          <p className={s('subtitle', 'is-marginless', 'is-6')}>{ user.fullName }</p>
          <p className={s('subtitle', 'is-marginless', 'is-6')}>{user.phone}</p>
          <p className={s('subtitle', 'is-5')}>{accounting.formatMoney(amount, '₦')}</p>
        </div>
        <button
          disabled={isPaid}
          onClick={() => this.showModalContent(benefs)}
          className={s({ 'is-success': !isPaid, 'is-info': isPaid }, 'button')}
        >{!isPaid ? 'Confirm' : isConfirmed ? 'Paid && Confirmed' : 'Paid && Pending Confimation'}</button>
      </div>
    )
  }

  render () {
    const { package: donationPackage, createdAt, beneficiaries, isPublished, isCompleted } = this.props
    return (
      <div className={s('benef-trans')}>
        <div className={s('flex', 'jc-sb', 'ai-c', 'p-y-10')}>
          <p className={s('title', 'is-4', 'is-marginless')}>{donationPackage.name && donationPackage.name.toUpperCase()} Package</p>
          <p className={s('subtitle', 'is-5', 'is-marginless')}>{accounting.formatMoney(donationPackage.amount, '₦')}</p>
        </div>
        <div className={s('flex', 'jc-sb', 'ai-c', 'p-y-10')}>
          <div className='flex fd-c'>
            <small className={s('small')}>{moment(createdAt).format('MMMM Do YYYY, h:mm:ss a')}</small>
            <p>Time Left </p><small className={s('small', 'text-danger')}>{!isCompleted && this.timeLeft}</small>
          </div>
          <div className={s({ 'is-danger': !isPublished, 'is-success': isPublished }, 'button')}>
            {!isPublished ? 'UnMatched' : 'Matched'}
          </div>
        </div>
        { isPublished && <small className={s('small', 'subtitle', 'is-5')}>Beneficiaries</small>}
        {isPublished && beneficiaries.map(this.rndrBeneficiaries)}
        {this.rndrModal()}
      </div>
    )
  }
}
