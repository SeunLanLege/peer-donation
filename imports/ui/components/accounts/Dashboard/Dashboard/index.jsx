//  @flow
import React, { PureComponent } from 'react'
import { observer, inject } from 'mobx-react'
import { accounting } from 'meteor/iain:accounting'

import { Withdrawals } from './Withdrawals'
import { Donations } from './Donations'
import { s } from '../../../../style'

import type { DonationProps, WithdrawalProps } from '../../../../../type'

@inject('store')
@observer
export default class Dashboard extends PureComponent {
  props: { store: { DashStore: Object } };

  rndrDonations = (el: DonationProps) => {
    const { _id } = el
    return <Donations key={_id} {...el} />
  }

  rndrWithdrawals = (el: WithdrawalProps) => {
    const { _id } = el
    return (<Withdrawals key={_id} {...el} />)
  }

  render () {
    const { DashStore } = this.props.store
    return (
      <main>
        <h1 className={s('subtitle', 'is-5')}>Dashboard</h1>
        <section className={s('columns')}>
          <section className={s('column', 'is-6-tablet')}>
            <div className={s('card', 'card-stats')}>
              <div data-background-color='purple' className={s('card-header')}>
                <i className='material-icons'>apps</i>
              </div>
              <div className={s('card-content')}>
                <p className={s('subtitle', 'is-5')}>Total Donations</p>
                <h1 className={s('title', 'is-1')}>{(accounting.formatMoney(DashStore.totalDonations, '₦', 0))}</h1>
              </div>
            </div>
          </section>
          <section className={s('column', 'is-6-tablet')}>
            <div className={s('card', 'card-stats')}>
              <div data-background-color='green' className={s('card-header')}>
                <i className='material-icons'>toc</i>
              </div>
              <div className={s('card-content')}>
                <p className={s('subtitle', 'is-5')}>Total Withdrawals</p>
                <h1 className={s('title', 'is-1')}>{(accounting.formatMoney(DashStore.totalWithdrawals, '₦', 0))}</h1>
              </div>
            </div>
          </section>
        </section>
        <section className={s('columns')}>
          <div className={s('column', 'is-6-tablet')}>
            <div className={s('card')}>
              <div data-background-color='purple' className={s('card-header')}>
                <h1 className={s('title', 'is-4')}>Pending Donations</h1>
              </div>
              <div className={s('card-content', 'benefs-trans-parent')}>
                {DashStore.uncompletedDonations.length
                  ? DashStore.uncompletedDonations.map(this.rndrDonations)
                  : <small className={s('subtitle', 'is-6')}>No Donations To Show</small>}
              </div>
            </div>
          </div>
          <div className={s('column', 'is-6-tablet')}>
            <div className={s('card')}>
              <div data-background-color='green' className={s('card-header')}>
                <h1 className={s('title', 'is-4')}>Withdrawals</h1>
              </div>
              <div className={s('card-content')}>
                {DashStore.uncompletedWithdrawals.length
                  ? DashStore.uncompletedWithdrawals.map(this.rndrWithdrawals)
                  : <small className={s('subtitle', 'is-6')}>No Withdrawals To Show</small>}
              </div>
            </div>
          </div>
        </section>
      </main>
    )
  }
}
