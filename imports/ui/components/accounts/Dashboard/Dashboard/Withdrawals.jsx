//  @flow
import React, { PureComponent } from 'react'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import uuid from 'uuid/v4'

import { Bert } from 'meteor/themeteorchef:bert'
import { accounting } from 'meteor/iain:accounting'

import { Method } from '../../../../../Api/Methods/Methods'
import { s } from '../../../../style'

import type { WithdrawalProps } from '/imports/type'

export type donorInterface = {
  donor: Object,
  donorTransId: string,
  amount: number,
  isPaid: boolean,
  isConfirmed: boolean
}

@inject('store')
@observer
export class Withdrawals extends PureComponent {
  props: WithdrawalProps;

  confirmPayment = async (donorTransId: string) => {
    const { _id } = this.props
    try {
      await Method('beneficiary.confirmPayment', { _id, donorTransId })
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    }
  }

  renderDonors = (donor : donorInterface) => {
    const { donor: user, donorTransId, amount, isPaid, isConfirmed, isBonusPackage } = donor
    return (
      <div style={{ margin: '30px 0px' }} className={s('flex', 'jc-sb', 'ai-c')} key={uuid()}>
        <div style={{ height: 80 }} className={s('flex', 'fd-c', 'jc-sb')}>
          <p className={s('subtitle', 'is-marginless', 'is-6')}>{ user.fullName }</p>
          <p className={s('subtitle', 'is-marginless', 'is-6')}>{user.phone}</p>
          <p className={s('subtitle', 'is-5')}>{accounting.formatMoney(amount, '₦')}</p>
        </div>
        <button
          disabled={!isPaid || isConfirmed}
          onClick={() => this.confirmPayment(donorTransId)}
          className={s('button', { 'is-primary': isPaid, 'is-danger': !isPaid })}>
          {!isPaid ? 'Unpaid' : isConfirmed ? 'Confirmed' : 'Paid'}
        </button>
      </div>
    )
  }

  render () {
    const { donors, package: donationPackage, amount, _id, isPublished, createdAt } = this.props
    return (
      <div className={s('benef-trans')}>
        <div className={s('flex', 'jc-sb', 'ai-c', 'p-y-10')}>
          <p className={s('title', 'is-4', 'is-marginless')}>{donationPackage.name.toUpperCase()} Package</p>
          <p className={s('subtitle', 'is-5', 'is-marginless')}>{
            donationPackage.isBonusPackage
            ? accounting.formatMoney(donationPackage.amount * donationPackage.percentage, '₦')
            : accounting.formatMoney(amount, '₦') }</p>
        </div>
        <div className={s('flex', 'jc-sb', 'ai-c', 'p-y-10')}>
          <small className={s('small')}>{moment(createdAt).format('MMMM Do YYYY, h:mm:ss a')}</small>
          <div className={s({ 'is-danger': !isPublished, 'is-success': isPublished }, 'button')}>
            {!isPublished ? 'UnMatched' : 'Matched'}
          </div>
        </div>
        { isPublished && <small className={s('small', 'subtitle', 'is-5')}>Donors</small>}
        {isPublished && donors.map(this.renderDonors)}
      </div>
    )
  }
}
