import React, { PureComponent } from 'react'
import { observer, inject } from 'mobx-react'
import { Donations } from '../Dashboard/Donations'
import { Withdrawals } from '../Dashboard/Withdrawals'
import { accounting } from 'meteor/iain:accounting'
import { chunk } from 'lodash'
import { DonationPackages } from '/imports/Classes/Packages'

import { Meteor } from 'meteor/meteor'

import { s } from '/imports/ui/style'

const style = {
  height: 'calc(100% - 130px)',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
}

@inject('store')
@observer
export default class TransactionHistoryUi extends PureComponent {

  rndrPackageItems = (donationPackage: IPackage) => {
    const { name, price, _id, description, percentage, duration, isBonusPackage } = donationPackage
    return (
      <div key={_id} className={s('card', 'column', 'is-4-tablet')}>
        <div data-background-color='green' className={s('card-header', 'flex', 'jc-sb', 'ai-c')}>
          <h1 className={s('title', 'is-4', 'is-paddingless', 'is-marginless')}>{name.toUpperCase()}</h1>
          <h1 className={s('title', 'is-4')}>{accounting.formatMoney(price * percentage, '₦')}</h1>
        </div>
        <div style={style} className={s('card-content')}>
          { description &&
            <div className={s('subtitle', 'is-6')}>
              {description}
            </div>}
          <div className={s('flex', 'jc-sb', 'fd-c')}>
            <div className={s('flex', 'jc-sb')}>
              <p className={s('subtitle', 'is-6')}>Percentage</p>
              <p className={s('subtitle', 'is-6')}>{percentage * 100}%</p>
            </div>
            <div className={s('flex', 'jc-sb')}>
              <p className={s('subtitle', 'is-6')}>Duration</p>
              <p className={s('subtitle', 'is-6')}>{duration} days</p>
            </div>
          </div>
        </div>
        <div className={s('card-footer', 'has-text-centered')}>
          <div style={{ marginBottom: 10 }} className={s('column', 'is-paddingless', 'flex', 'jc-sb')}>
            <div className={s('is-6-tablet')}>
              <button
                className={s('is-primary', 'is-fullwidth', 'button')}
                onClick={() => this.edit(_id)}>
                Edit Package</button>
            </div>
            <div className={s('is-6-tablet')}>
              <button
                className={s('is-danger', 'is-fullwidth', 'button')}
                onClick={() => this.deletePackage(_id)}>
                Delete Package</button>
            </div>
          </div>
          <small> Eligible for { !isBonusPackage
            ? accounting.formatMoney((price * percentage) + price, '₦', 0)
            : accounting.formatMoney(price * percentage, '₦', 0)} withdrawal after {duration} days</small>
        </div>
      </div>
    )
  }

  rndrPackageRow = (el, i) =>
    <section key={i} className={s('columns', 'is-marginless')}>
      {el.map(({ packageId }) => this.rndrPackageItems(DonationPackages.findOne(packageId)))}
    </section>

  rndrBonusPackages = () => {
    return Meteor.user() && Meteor.user().bonusPackages && Meteor.user().bonusPackages.length
      ? chunk(Meteor.user().bonusPackages, 3)
          .map(this.rndrPackageRow)
      : <p className={s('subtitle', 'is-6')}>Nothing To Show</p>
  }

  render () {
    const { DashStore } = this.props.store
    return (
      <main>
        <h1 className={s('subtitle', 'is-5')}>Transaction History</h1>
        <div className={s('columns')}>
          <div className={s('column', 'is-6-tablet')}>
            <div className={s('card')}>
              <div data-background-color='green' className={s('card-header')}>
                <h1 className={s('title', 'is-4')}>Donation History</h1>
              </div>
              <div className={s('card-content')}>
                {DashStore.completedDonations.length
                  ? DashStore.completedDonations.map((el, i) => <Donations key={i} {...el} />)
                  : <h1 className={s('subtitle', 'is-6')}>Nothing To Show</h1>
                }
              </div>
            </div>
          </div>
          <div className={s('column', 'is-6-tablet')}>
            <div className={s('card')}>
              <div data-background-color='purple' className={s('card-header')}>
                <h1 className={s('title', 'is-4')}>Withdrawal History</h1>
              </div>
              <div className={s('card-content')}>
                {DashStore.completedWithdrawals.length
                  ? DashStore.completedWithdrawals.map((el, i) => <Withdrawals key={i} {...el} />)
                  : <h1 className={s('subtitle', 'is-6')}>Nothing To Show</h1>
                }
              </div>
            </div>
          </div>
        </div>
        <h1 className={s('subtitle', 'is-5')}>Bonus Packages</h1>
        {this.rndrBonusPackages()}
      </main>
    )
  }
}
