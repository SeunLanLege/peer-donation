import React, { PureComponent, PropTypes } from 'react'
import cn from 'classnames/bind'
import isEqual from 'lodash/isEqual'
import { observer } from 'mobx-react'
import { observable, action } from 'mobx'

import { Bert } from 'meteor/themeteorchef:bert'

import { ChangePasswordMethod } from '../../../../../Api/Methods/Methods'
import styl from '../../../../stylus/index.styl'

const s = cn.bind(styl)

@observer
class ChangePasswordUI extends PureComponent {
  @observable data = {
    oldPassword: '',
    password: '',
    password2: ''
  }

  @observable isLoading = false

  @action toggleLoading (status) {
    this.isLoading = status
  }

  resetPassword = async (e) => {
    this.toggleLoading(true)
    e.preventDefault()
    if (isEqual(this.data.password, this.data.password2)) {
      try {
        await ChangePasswordMethod(this.data.oldPassword, this.data.password)
        Bert.alert('Password Changed Successfully!', 'success', 'growl-top-right')
      } catch (e) {
        Bert.alert(e.reason, 'danger', 'growl-top-right')
      } finally {
        this.toggleLoading(false)
      }
    } else {
      Bert.alert('Passwords Do Not Match!', 'danger', 'growl-top-right')
    }
  }

  @action onTextChange = ({ target }) => {
    const { name, value } = target
    this.data[name] = value
  }

  render () {
    const { password, password2 } = this.data
    return (
      <main className={s('columns', 'flex', 'jc-c', 'ai-c', 'flex-1')}>
        <section className={s('column', 'is-5-tablet')}>
          <form onSubmit={this.resetPassword} className={s('box')}>
            <label htmlFor='oldPassword'>Old Password</label>
            <input
              autoCorrect='off'
              value={this.data.oldPassword}
              onChange={this.onTextChange}
              className={s('input')}
              type='password' name='oldPassword' />
            <label htmlFor='password'>New Password</label>
            <input
              autoCorrect='off'
              value={this.data.password}
              onChange={this.onTextChange}
              className={s('input')}
              type='password' name='password' />
            <label htmlFor='password'>New Password</label>
            <input
              autoCorrect='off'
              value={this.data.password2}
              onChange={this.onTextChange}
              className={s('input')}
              type='password' name='password2' />
            <br /><br />
            <button
              disabled={!((password && password2) && (password === password2))}
              className={s('button', 'is-success', 'is-fullwidth', { 'is-loading': this.isLoading, 'is-disabled': this.isLoading })}>Submit</button>
          </form>
        </section>
      </main>
    )
  }
}

export default ChangePasswordUI
