import React, { PureComponent, PropTypes } from 'react'
import cn from 'classnames/bind'
import { observer, inject } from 'mobx-react'
import { observable, action } from 'mobx'

import { Bert } from 'meteor/themeteorchef:bert'

import { Method } from '../../../../../Api/Methods/Methods'
import styl from '../../../../stylus/index.styl'

const s = cn.bind(styl)

@inject('store')
@observer
class BankDetailsUI extends PureComponent {
  static propTypes = {
    store: PropTypes.object.isRequired
  }

  @observable isLoading = false

  @action toggleLoading (status) {
    this.isLoading = status
  }

  changeBankInfo = async (e) => {
    this.toggleLoading(true)
    const { User: { data } } = this.props.store
    const object = {
      bankAccName: data.bankAccName,
      bankName: data.bankName,
      bankAccNumber: data.bankAccNumber
    }
    e.preventDefault()
    try {
      await Method('users.updateInfo', object)
      Bert.alert('Bank Details Updated Successfully!', 'success', 'growl-top-right')
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.toggleLoading(false)
    }
  }

  onTextChange = (e) => {
    const { User } = this.props.store
    User.updateFields(e)
  }

  render () {
    const { User: { data } } = this.props.store
    return (
      <main className={s('columns', 'flex', 'jc-c', 'ai-c', 'flex-1')}>
        <section className={s('column', 'is-5-tablet')}>
          <form onSubmit={this.changeBankInfo} className={s('box')}>

            <label htmlFor='bankName'>Bank Name</label>
            <input
              autoCorrect='off'
              value={data.bankName}
              onChange={this.onTextChange}
              className={s('input')}
              type='text'
              name='bankName' />

            <label htmlFor='bankAccName'>Bank Account Name</label>
            <input
              autoCorrect='off'
              value={data.bankAccName}
              onChange={this.onTextChange}
              className={s('input')}
              type='text'
              name='bankAccName' />

            <label htmlFor='text'>Bank Account Number</label>
            <input
              autoCorrect='off'
              value={data.bankAccNumber}
              onChange={this.onTextChange}
              className={s('input')}
              type='text'
              name='bankAccNumber' />

            <br /><br />
            <button
              className={s('button', 'is-success', 'is-fullwidth', { 'is-loading': this.isLoading, 'is-disabled': this.isLoading })}>Submit</button>
          </form>
        </section>
      </main>
    )
  }
}

export default BankDetailsUI
