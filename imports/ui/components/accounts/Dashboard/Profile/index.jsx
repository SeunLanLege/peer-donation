import React, { PureComponent } from 'react'
// import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import cn from 'classnames/bind'

import ChangePassword from './ChangePassword'
import BankDetails from './BankDetails'
import EditProfile from './EditProfile'

import styl from '../../../../stylus/index.styl'

const s = cn.bind(styl)

class Profile extends PureComponent {
  constructor (props) {
    super(props)
    // Tabs.setUseDefaultStyles(false)
  }

  render () {
    return (
      <main>
        <h1 key='title' className={s('subtitle', 'is-5')}>Profile</h1>
        <div className={s('columns')}>
					<div className={s('column')}>
						<div className={s('card')}>
							<div className={s('card-header')} data-background-color='purple'>
								<h1 className={s('title', 'is-6')}>Edit Profile</h1>
							</div>
							<div className={s('card-content')}>
								<EditProfile />
							</div>
						</div>
					</div>
					<div className={s('column')}>
						<div className={s('card')}>
							<div className={s('card-header')} data-background-color='green'>
								<h1 className={s('title', 'is-6')}>Edit Bank Details</h1>
							</div>
							<div className={s('card-content')}>
								<BankDetails />
							</div>
						</div>
					</div>
				</div>
				<div className={s('columns')}>
					<div className={s('column', 'is-6-tablet')}>
						<div className={s('card')}>
							<div className={s('card-header')} data-background-color='blue'>
								<h1 className={s('title', 'is-6')}>Change Password</h1>
							</div>
							<div className={s('card-content')}>
								<ChangePassword />
							</div>
						</div>
					</div>
				</div>
      </main>
    )
  }
}
export default Profile
