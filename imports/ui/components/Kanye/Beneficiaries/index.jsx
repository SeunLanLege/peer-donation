//  @flow
import { Meteor } from 'meteor/meteor'
import React, { PureComponent } from 'react'
import { action, observable } from 'mobx'
import { take, includes } from 'lodash'
import { observer, inject } from 'mobx-react'
import { Images } from '/imports/Classes/Images'
import { Method } from '/imports/Api/Methods/Methods'
import { accounting } from 'meteor/iain:accounting'
import { Bert } from 'meteor/themeteorchef:bert'
import uuid from 'uuid/v4'

import { s } from '/imports/ui/style'

type daI = {
  benefId: string,
  donorTransId: string,
  search: string,
  filter: string,
  packageId: string,
  userId: string,
  isLoading: boolean
}

@inject('store')
@observer
export class BeneficiariesUI extends PureComponent {
  props: Object;
  @observable isModalOpen: boolean = false
  @observable data: daI = {
    benefId: '',
    isLoading: false,
    donorTransId: '',
    search: '',
    filter: '',
    packageId: '',
    userId: ''
  }

  @action toggleModal () {
    this.isModalOpen = !this.isModalOpen
  }

  @action setData = (key: string, value: any) => {
    this.data[key] = value
  }

  filterData = (data: Object) => {
    switch (this.data.filter) {
      case 'isCompleted':
        return data.isCompleted
      case 'isMatchedNotPublished':
        return data.isMatched && !data.isPublished
      case 'isMatched':
        return data.isMatched
      case 'isPublished':
        return data.isPublished
      case 'isUnMatched':
        return !data.isMatched
      default:
        return true
    }
  }

  togglePublish = async (id: string, status: boolean) => {
    this.setData('benefId', id)
    try {
      await Method('beneficiary.togglePublished', id, !status)
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setData('benefId', '')
    }
  }

  deleteDonor = async (donorTransId: string, benefTransId: string) => {
    this.setData('donorTransId', donorTransId)
    try {
      await Method('donor.delete', donorTransId, benefTransId)
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setData('donorTransId', '')
    }
  }

  confirmDonor = (donorTransId: string, benefTransId: string) => {

  }

  getThumbnailContent = (item: Object) => (
    <img
      key={item}
      className={s('image', 'is-70px')}
      style={{ width: '70px !important' }}
      src={item}
      alt={'confirmation'}
    />)

  confirmDonor = async (donorTransId: string, benefTransId: string) => {
    this.setData('benefTransId', benefTransId)
    try {
      await Method('beneficiary.confirmPayment', { _id: benefTransId, donorTransId })
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setData('benefTransId', '')
    }
  }

  returnItems = (el: string) => {
    const link = Images.findOne(el)
    if (link) {
      const blob = link.link()
      return {
        src: blob
      }
    }
    return {}
  }

  rndrDonors = (dnr: Object, benefTransId: string) => {
    const { DashStore } = this.props.store
    return (
      <div style={{margin: '15px 0px'}} key={uuid()}>
        <p className={s('flex', 'jc-sb', 'ai-c')}>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>Name</span>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>{dnr.donor && dnr.donor.fullName && dnr.donor.fullName}</span>
        </p>
        <p className={s('flex', 'jc-sb', 'ai-c')}>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>Amount</span>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>{accounting.formatMoney(dnr.amount, '₦', 0)}</span>
        </p>
        <div className={s('flex', 'jc-sb', 'ai-c')}>
          {
            !DashStore.isImagesLoading &&
              dnr.arrayOfImageId.map((el) => this.getThumbnailContent(this.returnItems(el).src))
          }
        </div>
        <p className={s('flex', 'jc-sb', 'ai-c')}>
          <button
            disabled={dnr.donorTransId === this.data.donorTransId}
            onClick={() => this.deleteDonor(dnr.donorTransId, benefTransId)}
            className={s('button', 'is-danger', {'is-loading': dnr.donorTransId === this.data.donorTransId})}
          >Delete Donor</button>
          <button
            disabled={!dnr.isPaid || dnr.isCompleted || dnr.isConfirmed}
            onClick={() => this.confirmDonor(dnr.donorTransId, benefTransId)}
            className={s('button', { 'is-success': dnr.isPaid, 'is-danger': !dnr.isPaid })}
          >{dnr.isCompleted ? 'Completed' : dnr.isConfirmed ? 'Confirmed' : dnr.isPaid ? 'Confirm Payment' : 'UnPaid'}</button>
        </p>
      </div>
    )
  }

  rndrBenef = (benef: Object) => {
    const { beneficiary, amount, amountLeft, donors, isCompleted, isMatched, isPublished, _id } = benef
    return (
      <tr key={_id}>
        <td className={s('has-text-centered')}>{Meteor.users.findOne(beneficiary) && Meteor.users.findOne(beneficiary).fullName}</td>
        <td className={s('has-text-centered')}>{accounting.formatMoney(amount, '₦', 0)}</td>
        <td className={s('has-text-centered')}>{isCompleted
          ? <div className={s('button', 'is-success')}>
              isCompleted
            </div>
          : isPublished
          ? <button
            disabled={_id === this.data.benefId}
            className={s('button', 'is-danger', { 'is-loading': _id === this.data.benefId })}
            onClick={() => this.togglePublish(_id, isPublished)}
            >UnPublish</button>
          : isMatched
          ? <button
            disabled={_id === this.data.benefId}
            className={s('button', 'is-success', { 'is-loading': _id === this.data.benefId })}
            onClick={() => this.togglePublish(_id, isPublished)}
            >Publish</button>
          : <div className={s('button', 'is-danger')}>
              UnMatched
            </div>
          }</td>
        <td className={s('has-text-centered')}>{donors.map((el) => this.rndrDonors(el, _id))}</td>
        <td className={s('has-text-centered')}>{accounting.formatMoney(amountLeft, '₦', 0)}</td>
      </tr>
    )
  }

  newBenef = async () => {
    this.setData('isLoading', true)
    try {
      await Method('beneficiary.new', this.data.userId, this.data.packageId)
      this.toggleModal()
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setData('isLoading', false)
    }
  }

  rndrModal = () => {
    if (this.isModalOpen) {
      const { Users, DashStore: { mainPackages } } = this.props.store
      return (
        <div className={s('modal', { 'is-active': this.isModalOpen })}>
          <div onClick={() => this.toggleModal()} className={s('modal-background')} />
          <div className={s('modal-card')}>
            <header className={s('modal-card-head')}>
              <p className={s('modal-card-title')}>Create A New Beneficiary</p>
              <button onClick={() => this.toggleModal()} className={s('delete')} />
            </header>
            <section className={s('modal-card-body')}>
              <form onSubmit={this.newBenef}>
                <label className={s('label')}>Select User</label>
                <div className={s('select')}>
                  <select
                    value={this.data.userId}
                    onChange={(e) => this.setData('userId', e.target.value)}>
                    <option value=''>Choose A User</option>
                    {Users.users.map(el => <option key={el._id} value={el._id}>{el.fullName && el.fullName}</option>)}
                  </select>
                </div>
                <label className={s('label')}>Bonus Package</label>
                <span className={s('select')}>
                  <select
                    value={this.data.packageId}
                    style={{ fontSize: '1rem' }}
                    onChange={(e) => this.setData('packageId', e.target.value)}>
                    <option value=''>Choose A Package</option>
                    {mainPackages &&
                      mainPackages.map(({ name, _id, price }) => (
                        <option key={_id} value={_id}>{`${name.toUpperCase()} - ${accounting.formatMoney(price, '₦', 0)}`}</option>
                    ))}
                  </select>
                </span>
              </form>
            </section>
            <footer className={s('modal-card-foot', 'flex', 'fd-c', 'ai-s')}>
              <button
                disabled={this.data.isLoading}
                onClick={this.newBenef}
                className={s('button', 'is-success', { 'is-loading': this.data.isLoading })}
              >Confirm</button>
            </footer>
          </div>
        </div>
      )
    }
  }

  render () {
    const { DashStore } = this.props.store
    const filtered = DashStore.withdrawals.filter(this.filterData)
    const searched = filtered.length
    ? filtered.filter(el => includes(Meteor.users.findOne(el.beneficiary) && Meteor.users.findOne(el.beneficiary).fullName.toLowerCase(), this.data.search.toLowerCase()))
    : filtered
    return (
      <main>
        <div className={s('flex', 'jc-sb', 'ai-c')}>
          <h1 className={s('subtitle', 'is-5')}>Withdrawals</h1>
          <button
            onClick={() => this.toggleModal()}
            className={s('button', 'is-primary')}>Add a New Beneficiary</button>
        </div>
        <div className={s('card')}>
          <div data-background-color='origin' className={s('card-header')}>
            <div className={s('flex', 'jc-sb', 'fd-c')}>
              <h1 className={s('title', 'is-4', 'is-marginless')}>Withdrawals</h1>
              <input
                style={{ maxWidth: 400, margin: '10px 0px' }}
                value={this.data.search}
                onChange={(e) => this.setData('search', e.target.value)}
                type='text'
                className={s('input')}
                placeholder='search by name' />
              <div className={s('select')}>
                <select name='filter' id='' value={this.data.filter} onChange={(e) => this.setData('filter', e.target.value)}>
                  <option value=''>Choose Filter</option>
                  <option value='isCompleted'>Show Completed</option>
                  <option value='isPublished'>Show Published</option>
                  <option value='isMatchedNotPublished'>Show Matched But Not Published</option>
                  <option value='isMatched'>Show Matched</option>
                  <option value='isUnMatched'>Show UnMatched</option>
                </select>
              </div>
            </div>
          </div>
          <div className={s('card-content')}>
            <table className={s('table')}>
              <thead>
                <tr>
                  <th className={s('has-text-centered')}>Name</th>
                  <th className={s('has-text-centered')}>Amount</th>
                  <th className={s('has-text-centered')}>Actions</th>
                  <th className={s('has-text-centered')}>Donors</th>
                  <th className={s('has-text-centered')}>Amount Left</th>
                </tr>
              </thead>
              <tbody>
                { take(searched, 50).map(this.rndrBenef) }
              </tbody>
            </table>
          </div>
        </div>
        {this.rndrModal()}
      </main>
    )
  }
}
