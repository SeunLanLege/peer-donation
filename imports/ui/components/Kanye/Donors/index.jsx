//  @flow
import { Meteor } from 'meteor/meteor'
import React, { PureComponent } from 'react'
import { action, observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { Images } from '/imports/Classes/Images'
import { Method } from '/imports/Api/Methods/Methods'
import { take, includes } from 'lodash'
import { accounting } from 'meteor/iain:accounting'
import { Bert } from 'meteor/themeteorchef:bert'
import uuid from 'uuid/v4'

import { s } from '/imports/ui/style'

type Itarget = { target: HTMLInputElement };

@inject('store')
@observer
export class DonorsUI extends PureComponent {
  props: Object;
  @observable filter: string = ''
  @observable search: string = ''
  @observable isOpen = true
  @observable data: { donorTransId: string, donorTransId: string, benefTransId: string } = {
    donorTransId: '',
    benefTransId: ''
  }

  @action searchT = (e: { target: HTMLInputElement }) => {
    const { target } = e
    this.search = target.value
  }

  @action toggle () {
    this.isOpen = !this.isOpen
  }

  @action setData = (key: string, value: string) => {
    this.data[key] = value
  }

  @action change = (e: Itarget) => {
    const { target } = e
    this.filter = target.value
  }

  filterData = (data: Object) => {
    switch (this.filter) {
      case 'isCompleted':
        return data.isCompleted
      case 'isMatchedNotPublished':
        return data.isMatched && !data.isPublished
      case 'isMatched':
        return data.isMatched
      case 'isPublished':
        return data.isPublished
      case 'isUnMatched':
        return !data.isMatched
      default:
        return true
    }
  }

  togglePublish = async (id: string, status: boolean) => {
    this.setData('donorTransId', id)
    try {
      await Method('donor.togglePublished', id, !status)
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setData('donorTransId', '')
    }
  }

  deleteDonor = async (donorTransId: string) => {
    this.setData('donorTransId', donorTransId)
    try {
      await Method('donor.delete', donorTransId)
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setData('donorTransId', '')
    }
  }

  getThumbnailContent = (item: Object) =>
    <img
      onClick={this.toggle}
      key={item}
      className={s('image', 'is-70px')}
      style={{ width: '70px !important' }}
      src={item}
      alt={'confirmation'}
    />

  confirmDonor = async (donorTransId: string, benefTransId: string) => {
    this.setData('benefTransId', benefTransId)
    try {
      await Method('beneficiary.confirmPayment', { _id: benefTransId, donorTransId })
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.setData('benefTransId', '')
    }
  }

  returnItems = (el: string) => {
    const link = Images.findOne(el)
    if (link) {
      const blob = link.link()
      return {
        src: blob
      }
    }
    return {}
  }

  rndrDonors = (bnf: Object, donorTransId: string) => {
    const { DashStore } = this.props.store
    return (
      <div style={{margin: '15px 0px'}} key={uuid()}>
        <p className={s('flex', 'jc-sb', 'ai-c')}>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>Name</span>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>{bnf.beneficiary && bnf.beneficiary.fullName && bnf.beneficiary.fullName}</span>
        </p>
        <p className={s('flex', 'jc-sb', 'ai-c')}>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>Amount</span>
          <span className={s('subtitle', 'is-6', 'is-marginless')}>{accounting.formatMoney(bnf.amount, '₦', 0)}</span>
        </p>
        <div className={s('flex', 'jc-sb', 'ai-c')}>
          {
            !DashStore.isImagesLoading &&
              bnf.arrayOfImageId.map((el) => this.getThumbnailContent(this.returnItems(el).src))
          }
        </div>
        <div className={s('flex', 'jc-e', 'ai-c')}>
          <button
            disabled={(this.data.benefTransId === bnf.benefTransId) || !bnf.isPaid || bnf.isConfirmed}
            onClick={() => this.confirmDonor(donorTransId, bnf.benefTransId)}
            className={s('button', { 'is-success': bnf.isPaid, 'is-danger': !bnf.isPaid, 'is-loading': this.data.benefTransId === bnf.benefTransId })}
          >{bnf.isConfirmed ? 'Confirmed' : bnf.isPaid ? 'Paid' : 'UnPaid'}
          </button>
        </div>
      </div>
    )
  }

  rndrBenef = (donor: Object) => {
    const { donor: user, package: amount, amountLeft, beneficiaries, isCompleted, isMatched, isPublished, _id } = donor
    return (
      <tr key={_id}>
        <td className={s('has-text-centered')}>{Meteor.users.findOne(user) && Meteor.users.findOne(user).fullName}</td>
        <td className={s('has-text-centered')}>{accounting.formatMoney(amount.amount, '₦', 0)}</td>
        <td className={s('has-text-centered')}>{isCompleted
          ? <div className={s('button', 'is-success')}>
              isCompleted
            </div>
          : isPublished
          ? <button
            disabled={_id === this.data.donorTransId}
            className={s('button', 'is-danger', { 'is-loading': _id === this.data.donorTransId })}
            onClick={() => this.togglePublish(_id, isPublished)}
            >UnPublish</button>
          : isMatched
          ? <button
            disabled={_id === this.data.donorTransId}
            className={s('button', 'is-success', { 'is-loading': _id === this.data.donorTransId })}
            onClick={() => this.togglePublish(_id, isPublished)}
            >Publish</button>
          : <div className={s('button', 'is-danger')}>
              UnMatched
            </div>
          }</td>
        <td className={s('has-text-centered')}>{beneficiaries.map((el) => this.rndrDonors(el, _id))}</td>
        <td className={s('has-text-centered')}>{accounting.formatMoney(amountLeft, '₦', 0)}</td>
      </tr>
    )
  }

  render () {
    const { DashStore } = this.props.store
    const filtered = DashStore.donations.filter(this.filterData)
    const searched = filtered.length
    ? filtered.filter(el => includes(Meteor.users.findOne(el.donor) && Meteor.users.findOne(el.donor).fullName.toLowerCase(), this.search.toLowerCase()))
    : filtered
    return (
      <main>
        <h1 className={s('subtitle', 'is-5')}>Donations</h1>
        <div className={s('card')}>
          <div data-background-color='origin' className={s('card-header')}>
            <div className={s('flex', 'jc-sb', 'fd-c')}>
              <h1 className={s('title', 'is-4', 'is-marginless')}>Donations</h1>
              <input
                style={{ maxWidth: 400, margin: '10px 0px' }}
                value={this.search}
                onChange={this.searchT}
                type='text'
                className={s('input')}
                placeholder='search by name' />
              <div className={s('select')}>
                <select name='filter' id='' value={this.filter} onChange={this.change}>
                  <option value=''>Choose Filter</option>
                  <option value='isCompleted'>Show Completed</option>
                  <option value='isPublished'>Show Published</option>
                  <option value='isMatchedNotPublished'>Show Matched But Not Published</option>
                  <option value='isMatched'>Show Matched</option>
                  <option value='isUnMatched'>Show UnMatched</option>
                </select>
              </div>
            </div>
          </div>
          <div className={s('card-content')}>
            <table className={s('table')}>
              <thead>
                <tr>
                  <th className={s('has-text-centered')}>Name</th>
                  <th className={s('has-text-centered')}>Amount</th>
                  <th className={s('has-text-centered')}>Actions/ Status</th>
                  <th className={s('has-text-centered')}>Beneficiaries</th>
                  <th className={s('has-text-centered')}>Amount Left</th>
                </tr>
              </thead>
              <tbody>
                { take(searched, 50).map(this.rndrBenef) }
              </tbody>
            </table>
          </div>
        </div>
      </main>
    )
  }
}
