//  @flow
import React, { PureComponent } from 'react'
import { observer, inject } from 'mobx-react'
import { observable, action , IDerivationState} from 'mobx'

import { Bert } from 'meteor/themeteorchef:bert'
import { accounting } from 'meteor/iain:accounting'
import { find, chunk } from 'lodash'

import { Method } from '../../../../Api/Methods/Methods'
import { s } from '../../../style'
import {Beneficiaries} from '../../../../Classes/Beneficiaries';

interface IPackage {
  name: string,
  isBonusPackage: boolean,
  price: number,
  _id: string,
  description: string,
  duration: number,
  percentage: number,
  reward: number,
  parentId: ?string
}

type Props = { store: Object };

const style = {
  height: 'calc(100% - 130px)',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
}

@inject('store')
@observer
export class DonationPackagesUI extends PureComponent {
  props: Props;

  init = {
    name: '',
    price: '',
    description: '',
    percentage: '',
    duration: '',
    parentId: '',
    isBonusPackage: '0'
  }

  @observable isModalOpen: boolean = false
  @observable isInvalid: boolean = true
  @observable data: Object = this.init
  @observable isLoading: boolean = false

	@observable packages = 0
	@observable	donors = 0
	@observable	beneficiaries = 0
	@observable	totalDonations = 0

  @action toggleModal = () => {
    this.isModalOpen = !this.isModalOpen
  }

  @action setData = (data: Object) => {
    this.data = { ...data }
  }

  @action toogleisInvalid = (status: boolean) => {
    this.isInvalid = status
  }

	@action setKey = (key, value) => {
		this[key] = value
	}

  @action toggleLoading = () => {
    this.isLoading = !this.isLoading
  }

  @action change = (e: { target: HTMLInputElement }) => {
    const { target } = e
    this.data[target.name] = target.type === 'number' ? parseInt(target.value, 10) : target.value
    const keys = Object.keys(this.data)
    let error = 0
    for (let key of keys) {
      if (!this.data[key] &&
        (key !== '_id' &&
          key !== 'parentId' &&
          key !== 'reward' &&
          key !== '_isNew' &&
          key !== 'isBonusPackage')) {
        error += 1
      }
    }
    error === 0 ? this.toogleisInvalid(false) : this.toogleisInvalid(true)
  }

	changePackage = (e: { target: HTMLInputElement }) => {
		this.setKey(e.target.name, e.target.value)
	}

	componentDidMount() {
		this.getStats()
	}

	getStats = async () => {
		try {
			const data = await Method('stats.get')
			Object.keys(data).forEach(el => {
				this.setKey(el, data[el])
			})
		} catch (e) {
			console.log('Error :', JSON.stringify(e, null, 4))
		}
	}

	sendStats = async (e) => {
		 e.preventDefault && e.preventDefault()
		const { donors, packages, beneficiaries, totalDonations } = this
		let stats = { donors, packages, beneficiaries, totalDonations }
		Object.keys(stats).forEach(key => {
			stats[key] = parseInt(stats[key])
		})
		try {
			const data = await Method('stats.insert', stats)
      Bert.alert('Edited Statistics Successfully!', 'success', 'growl-top-right')			
		} catch (e) {
			console.log('Error send :', JSON.stringify(e, null, 4))
		}
	}

  edit = (id: string) => {
    const { DashStore } = this.props.store
    const data = find(DashStore.donationPackages, el => el._id === id)
    if (data) {
      const newData = { ...data, isBonusPackage: String(Number(data.isBonusPackage)) }
      this.setData(newData)
      this.toggleModal()
    }
  }

  createPackage = async () => {
    this.toggleLoading()
    const data = { ...this.data, percentage: this.data.percentage / 100 }
    data.isBonusPackage = Boolean(Number(data.isBonusPackage))
    try {
      await Method('packages.create', data)
      Bert.alert('Created Package Successfully!', 'success', 'growl-top-right')
      this.toggleModal()
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.toggleLoading()
    }
  }

  editPackage = async () => {
    this.toggleLoading()
    const data = this.data.percentage > 1 ? { ...this.data, percentage: this.data.percentage / 100 } : { ...this.data }
    data.isBonusPackage = Boolean(Number(data.isBonusPackage))
    const id = data._id
    delete data._isNew
    delete data.updatedAt
    delete data.createdAt
    delete data.reward
    try {
      await Method('packages.edit', id, data)
      Bert.alert('Edited Package Successfully!', 'success', 'growl-top-right')
      this.toggleModal()
      this.setData(this.init)
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.toggleLoading()
    }
  }

  deletePackage = async (id: string) => {
    const deletePackage = confirm('Are you sure you want to delete this package?')
    if (deletePackage) {
      try {
        await Method('packages.delete', id)
        Bert.alert('Deleted Package Successfully!', 'success', 'growl-top-right')
      } catch (e) {
        Bert.alert(e.reason, 'danger', 'growl-top-right')
      }
    }
  }

  rndrModal = () => {
    if (this.isModalOpen) {
      const { DashStore: { bonusPackages } } = this.props.store
      return (
        <div className={s('modal', { 'is-active': this.isModalOpen })}>
          <div onClick={() => this.toggleModal()} className={s('modal-background')} />
          <div className={s('modal-card')}>
            <header className={s('modal-card-head')}>
              <p className={s('modal-card-title')}>New Package</p>
              <button onClick={() => this.toggleModal()} className={s('delete')} />
            </header>
            <section className={s('modal-card-body')}>
							<form onSubmit={this.sendStats}>
								<label className={s('label')}>Package Name</label>
								<input
									onChange={this.change}
									maxLength={8}
									className={s('input')}
									type='text'
									name='name' value={this.data.name} />
								<label className={s('label')}>Package Price</label>
								<input
									onChange={this.change}
									className={s('input')}
									type='number'
									name='price' value={this.data.price} />
								<label className={s('label')}>Package Duration <small>(in days)</small></label>
								<input
									onChange={this.change}
									className={s('input')}
									type='number'
									name='duration' value={this.data.duration} />
								<label className={s('label')}>Package Percentage</label>
								<input
									onChange={this.change}
									className={s('input')}
									type='number'
									name='percentage' value={this.data.percentage} />
								<label className={s('label')}>Is Bonus Package ? </label>
								<div className={s('select')}>
									<select
										value={this.data.isBonusPackage}
										style={{ fontSize: '1rem' }}
										name='isBonusPackage'
										onChange={this.change}
									>
										<option value='1'>Yes</option>
										<option value='0'>No</option>
									</select>
								</div>
                {this.data.isBonusPackage === '0'
                  ? <div>
                    <label className={s('label')}>Bonus Package</label>
                    <span className={s('select')}>
                      <select
                        value={this.data.parentId}
                        style={{ fontSize: '1rem' }}
                        name='parentId'
                        onChange={this.change}
                      >
                        <option value=''>Choose A Bonus Package</option>
                        {bonusPackages &&
                          bonusPackages.map(({ name, _id, price }: IPackage) => (
                            <option key={_id} value={_id}>{`${name.toUpperCase()} - ${accounting.formatMoney(price, '₦', 0)}`}</option>
                        ))}
                      </select>
                    </span>
                  </div>
                : null}
                <label className={s('label')}>Package Description</label>
                <textarea
                  onChange={this.change}
                  className={s('textarea')}
                  type='text'
                  name='description'
                  value={this.data.description} />
              </form>
            </section>
            <footer className={s('modal-card-foot', 'flex', 'fd-c', 'ai-s')}>
              <button
                disabled={this.isInvalid || this.isLoading}
                onClick={this.data._id ? this.editPackage : this.createPackage}
                className={s('button', 'is-success', { 'is-loading': this.isUploading || this.isLoading })}
              >Confirm</button>
            </footer>
          </div>
        </div>
      )
    }
  }

  rndrPackageItems = (donationPackage: IPackage) => {
    const { name, price, _id, description, percentage, duration, isBonusPackage } = donationPackage
    return (
      <div key={_id} className={s('card', 'column', 'is-4-tablet')}>
        <div data-background-color='green' className={s('card-header', 'flex', 'jc-sb', 'ai-c')}>
          <h1 className={s('title', 'is-4', 'is-paddingless', 'is-marginless')}>{name.toUpperCase()}</h1>
          <h1 className={s('title', 'is-4')}>{accounting.formatMoney(price, '₦')}</h1>
        </div>
        <div style={style} className={s('card-content')}>
          { description &&
            <div className={s('subtitle', 'is-6')}>
              {description}
            </div>}
          <div className={s('flex', 'jc-sb', 'fd-c')}>
            <div className={s('flex', 'jc-sb')}>
              <p className={s('subtitle', 'is-6')}>Percentage</p>
              <p className={s('subtitle', 'is-6')}>{percentage * 100}%</p>
            </div>
            <div className={s('flex', 'jc-sb')}>
              <p className={s('subtitle', 'is-6')}>Duration</p>
              <p className={s('subtitle', 'is-6')}>{duration} days</p>
            </div>
          </div>
        </div>
        <div className={s('card-footer', 'has-text-centered')}>
          <div style={{ marginBottom: 10 }} className={s('column', 'is-paddingless', 'flex', 'jc-sb')}>
            <div className={s('is-6-tablet')}>
              <button
                className={s('is-primary', 'is-fullwidth', 'button')}
                onClick={() => this.edit(_id)}>
                Edit Package</button>
            </div>
            <div className={s('is-6-tablet')}>
              <button
                className={s('is-danger', 'is-fullwidth', 'button')}
                onClick={() => this.deletePackage(_id)}>
                Delete Package</button>
            </div>
          </div>
          <small> Eligible for { !isBonusPackage
            ? accounting.formatMoney((price * percentage) + price, '₦', 0)
            : accounting.formatMoney(price * percentage, '₦', 0)} withdrawal after {duration} days</small>
        </div>
      </div>
    )
  }

  rndrPackages = () => {
    const { DashStore } = this.props.store
    return !DashStore.isDonationPackageLoading &&
      chunk(DashStore.donationPackages, 3)
        .map((el, i) =>
          <section key={i} className={s('columns', 'is-marginless')}>
            {el.map(this.rndrPackageItems)}
          </section>
        )
  }

  render () {
    return (
      <main>
        <div className={s('flex', 'jc-sb', 'ai-c')}>
          <h1 className={s('subtitle', 'is-5')}>Packages</h1>
          <button
            onClick={() => this.toggleModal()}
            className={s('button', 'is-primary')}>Add a New Package</button>
        </div>
        {this.rndrPackages()}
        {this.rndrModal()}
				<div className={s('card', 'flex', 'jc-c', 'ai-c', 'fd-c')} style={{padding: '40px 0px'}}>
					<h1 className={s('subtitle', 'is-4')}>Edit Statistics</h1>
					<form onSubmit={this.sendStats}>
						<label className={s('label')}>Packages</label>
						<input
							onChange={this.changePackage}
							maxLength={8}
							className={s('input')}
							type='text'
							name='packages'
							value={this.packages} />
						<label className={s('label')}>Total Donors</label>
						<input
							onChange={this.changePackage}
							className={s('input')}
							type='number'
							name='donors' value={this.donors} />
						<label className={s('label')}>Total Beneficiaries <small>(in days)</small></label>
						<input
							onChange={this.changePackage}
							className={s('input')}
							type='number'
							name='beneficiaries' value={this.beneficiaries} />
						<label className={s('label')}>Total Donations</label>
						<input
							onChange={this.changePackage}
							className={s('input')}
							type='number'
							name='totalDonations' value={this.totalDonations} />
						<button
							onClick={this.sendStats}
							className={s('is-primary', 'is-fullwidth', 'button')}
						>Update Stats</button>
					</form>
				</div>
      </main>
    )
  }
}
