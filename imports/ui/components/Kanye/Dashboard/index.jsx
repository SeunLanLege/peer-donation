//  @flow
import React, { PureComponent } from 'react'
import { observer, inject } from 'mobx-react'
import { accounting } from 'meteor/iain:accounting'

import { Users } from './Users'
// import { Withdrawals } from './Withdrawals'
// import { Donations } from './Donations'
import { s } from '../../../style'

const subtitle = {
  color: '#999',
  fontSize: 15
}

@inject('store')
@observer
export class Dashboard extends PureComponent {
  props: Object

  render () {
    const { DashStore } = this.props.store
    return (
      <main>
        <h1 className={s('subtitle', 'is-5')}>Dashboard</h1>
        <section className={s('columns')}>
          <section className={s('column', 'is-3-tablet')}>
            <div className={s('card', 'card-stats')}>
              <div data-background-color='purple' className={s('card-header')}>
                <i className='material-icons'>apps</i>
              </div>
              <div className={s('card-content')}>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Total</p>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Donors</p>
                <h1 style={{ marginTop: 15 }} className={s('title', 'is-5')}>{DashStore.completedDonations.length}</h1>
              </div>
            </div>
          </section>
          <section className={s('column', 'is-3-tablet')}>
            <div className={s('card', 'card-stats')}>
              <div data-background-color='purple' className={s('card-header')}>
                <i className='material-icons'>apps</i>
              </div>
              <div className={s('card-content')}>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Total</p>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Donations</p>
                <h1 style={{ marginTop: 15 }} className={s('title', 'is-5')}>{(accounting.formatMoney(DashStore.totalDonations, '₦', 0))}</h1>
              </div>
            </div>
          </section>
          <section className={s('column', 'is-3-tablet')}>
            <div className={s('card', 'card-stats')}>
              <div data-background-color='purple' className={s('card-header')}>
                <i className='material-icons'>apps</i>
              </div>
              <div className={s('card-content')}>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Total</p>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Beneficiaries</p>
                <h1 style={{ marginTop: 15 }} className={s('title', 'is-5')}>{DashStore.completedWithdrawals.length}</h1>
              </div>
            </div>
          </section>
          <section className={s('column', 'is-3-tablet')}>
            <div className={s('card', 'card-stats')}>
              <div data-background-color='green' className={s('card-header')}>
                <i className='material-icons'>toc</i>
              </div>
              <div className={s('card-content')}>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Total</p>
                <p style={subtitle} className={s('subtitle', 'is-marginless', 'is-6')}>Withdrawals</p>
                <h1 style={{ marginTop: 15 }} className={s('title', 'is-5')}>{(accounting.formatMoney(DashStore.totalWithdrawals, '₦', 0))}</h1>
              </div>
            </div>
          </section>
        </section>
        <Users />
      </main>
    )
  }
}
