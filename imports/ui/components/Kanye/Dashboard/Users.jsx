import React, { PureComponent } from 'react'
import { observer, inject } from 'mobx-react'
import { action, observable } from 'mobx'
import { includes, take } from 'lodash'

import { Bert } from 'meteor/themeteorchef:bert'
import { Method } from '/imports/Api/Methods/Methods'
import { s } from '/imports/ui/style'

@inject('store')
@observer
export class Users extends PureComponent {
  props: { store: { Users: {} } };
  @observable search: string = '';
  @observable id: string = ''
  @observable filter: string = ''
  @action filterChange = (e: Itarget) => {
    const { target } = e
    this.filter = target.value
  }

  filterData = (data: Object) => {
    switch (this.filter) {
      case 'isBanned':
        return data.isBanned
      case 'isUnBanned':
        return !data.isBanned
      default:
        return true
    }
  }

  @action change = (e: { target: HTMLInputElement }) => {
    const { target } = e
    this.search = target.value
  }

  @action toggleId = (id) => {
    this.id = id
  }

  toggleBan = async (id) => {
    this.toggleId(id)
    try {
      await Method('users.toggleBan', id)
    } catch (e) {
      Bert.alert(e.reason, 'danger', 'growl-top-right')
    } finally {
      this.toggleId('')
    }
  }

  rndrUsers = (user: Object) => {
    return (
      <tr key={user._id}>
        <td className={s('subtitle', 'is-6')}>{user.fullName}</td>
        <td className={s('subtitle', 'is-6')}>{user.emails[0].address}</td>
        <td className={s('subtitle', 'is-6')}>{user.phone}</td>
        <td className={s('subtitle', 'is-6')}>{user.bankAccName}</td>
        <td className={s('subtitle', 'is-6')}>{user.bankAccNumber}</td>
        <td className={s('subtitle', 'is-6')}>{user.bankName}</td>
        <td>
          <button
            disabled={this.id === user._id}
            onClick={() => this.toggleBan(user._id)}
            className={s('button', 'is-fullwidth', {
              'is-loading': this.id === user._id,
              'is-primary': user.isBanned,
              'is-danger': !user.isBanned })}
            >{user.isBanned ? 'UnBan User' : 'Ban User'}</button>
        </td>
      </tr>
    )
  }

  render () {
    const { Users } = this.props.store
    const filtered = Users.users.filter(this.filterData)
    const users = this.search && filtered.length
      ? filtered.filter(el =>
        (includes(el.fullName && el.fullName.toLowerCase(), this.search.toLowerCase()) ||
          includes(el.emails[0].address && el.emails[0].address.toLowerCase(), this.search.toLowerCase())))
      : filtered
    return (
      <main>
        <div className={s('card')}>
          <div data-background-color='origin' className={s('card-header')}>
            <div className={s('flex', 'fd-c')}>
              <h1 className={s('title', 'is-5')}>Users</h1>
              <input
                style={{ maxWidth: 400, margin: '10px 0px' }}
                placeholder='Search Users by name or email'
                type='text'
                className={s('input')}
                value={this.search}
                onChange={this.change} />
              <div className={s('select')}>
                <select value={this.filter} onChange={this.filterChange} name='filter' id=''>
                  <option value=''>Choose a filter</option>
                  <option value='isUnBanned'>Show UnBanned Users</option>
                  <option value='isBanned'>Show Banned Users</option>
                </select>
              </div>
            </div>
          </div>
          <table className={s('table')}>
            <thead>
              <tr>
                <th>Name</th>
                <th>Email Address</th>
                <th>Phone</th>
                <th>Account Name</th>
                <th>Accunt Number</th>
                <th>Bank Name</th>
                <th>actions</th>
              </tr>
            </thead>
            <tbody>
              { take(users, 50).map(this.rndrUsers) }
            </tbody>
          </table>
        </div>
      </main>
    )
  }
}
