import React, { PropTypes, PureComponent } from 'react'
import { Provider, observer } from 'mobx-react'
import cn from 'classnames/bind'
import isEqual from 'lodash/isEqual'
import { observable, action } from 'mobx'

import store from '../../store'
import styl from '../stylus/index.styl'
import LogoutButton from '../components/accounts/Auths/Logout'

const s = cn.bind(styl)

@observer
class MainLayout extends PureComponent {
  static propTypes = {
    content: PropTypes.element.isRequired
  }

  @observable isActive = false

  @action toggleActive = () => {
    this.isActive = !this.isActive
  }


  render () {
    const { content } = this.props
    const { userId, currentRoute } = store.User
    const { isFixed } = store.Misc
    return (
      <Provider
        store={store}>
        <main>
          <link rel='icon' href='/favicon.ico' type='image/x-icon' />
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta name='fragment' content='!' />
          <title>PEER DONATION: Achieve Financial Freedom</title>
          <nav className={s('nav', 'zI9', {
            'is-fixed-top': isFixed,
            'is-transparent': (!isFixed && isEqual(currentRoute.path, '/') || !isEqual(currentRoute.path, '/')),
            'is-absolute': !isFixed,
            'top': !isFixed,
            'has-shadow': isFixed || !isEqual(currentRoute.path, '/'),
            'is-white': (isFixed && isEqual(currentRoute.path, '/') || !isEqual(currentRoute.path, '/')),
            'fadeInDown': isFixed
          })}>
            <div className={s('container')}>
              <span onClick={this.toggleActive} className={s('nav-toggle', { 'is-active': this.isActive })}>
                <span />
                <span />
                <span />
              </span>
              <div className={s('nav-left')}>
                <a className={s('nav-item')}>
                  <img src='/logo.png' alt='PEER DONATION logo' />
									<span className={s('logo-text', 'is-subtitle', 'is-4', { 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') })}>Peer Donation</span>
                </a>
              </div>
              <div style={{ zIndex: 99 }} className={s('nav-right', 'nav-menu', { 'is-active': this.isActive })}>
                <a href='/' onClick={this.toggleActive} className={s('nav-item', 'is-hidden-tablet', { 'is-active': isEqual(currentRoute.path, '/') })}>Home</a>
                <a href='/#aboutus' onClick={this.toggleActive} className={s('nav-item', 'is-hidden-tablet')}>About Us</a>
                <a href='/howItWorks' onClick={this.toggleActive} className={s('nav-item', 'is-hidden-tablet')}>How It Works</a>
                { !userId ? [
                  <a onClick={this.toggleActive} key='login' href='/login' className={s('nav-item', 'is-hidden-tablet', { 'is-active': isEqual(currentRoute.path, '/login') })}>Login</a>,
                  <a onClick={this.toggleActive} key='register' href='/register' className={s('nav-item', 'is-hidden-tablet', { 'is-active': isEqual(currentRoute.path, '/register') })}>Register</a>
                ] : [
                  <a onClick={this.toggleActive} key='dashboard' href='/account/dashboard' className={s('nav-item', 'is-hidden-tablet', { 'is-active': isEqual(currentRoute.path, '/account/dashboard') })}>Dashboard</a>,
                  <LogoutButton key='logout' className={s('is-primary', 'is-outlined', 'is-hidden-tablet', 'nav-item')}>Log Out</LogoutButton>
                ] }
                <div className={s('nav-right', 'nav-menu')}>
                  <a href='/' className={s('nav-item', 'is-tab', { 'is-active': isEqual(currentRoute.path, '/'), 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') })}>
                    Home
                  </a>
                  <a href='/#aboutus' className={s('nav-item', 'is-tab', { 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') })}>
                    About Us
                  </a>
                  <a href='/howItWorks' className={s('nav-item', 'is-tab', { 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') })}>
                    How It Works
                  </a>
                  {!userId ? [
                    <a
                      key='login'
                      href='/login'
											style={{ marginRight: 10, marginTop: 10 }}
                      className={s('nav-item', 'button', 'is-primary', { 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') }, 'is-primary', { 'is-active': isEqual(currentRoute.path, '/login') })}>
                      <span>Sign In</span>
                    </a>,
                    <a
                      key='register'
                      href='/register'
											style={{ marginTop: 10 }}
                      className={s('nav-item', 'button', 'is-info', { 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') }, 'is-primary', { 'is-active': isEqual(currentRoute.path, '/register') })}>
                      <span>Sign Up</span>
                    </a> ] : [
                      <a
                        key='account'
                        href='/account/dashboard'
                        className={s('nav-item', { 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') }, 'is-primary', 'is-outlined', 'is-tab', { 'is-active': isEqual(currentRoute.path, '/account/dashboard') })}>
                        <span>Dashboard</span>
                      </a>,
                      <LogoutButton key='logout' className={s('is-primary', { 'is-text-white': !isFixed && isEqual(currentRoute.path, '/') }, 'is-outlined', 'nav-item')}>Log Out</LogoutButton>
                    ]
                  }
                </div>
              </div>
            </div>
          </nav>
          {content}
        </main>
      </Provider>
    )
  }
}

export default MainLayout
