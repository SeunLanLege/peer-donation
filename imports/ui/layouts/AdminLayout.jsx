//   @flow
import React, { PureComponent } from 'react'
import { observer, Provider } from 'mobx-react'
import { observable, action } from 'mobx'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { OffCanvas, OffCanvasMenu, OffCanvasBody } from 'react-offcanvas'

import { s } from '../style'
import store from '../../store'
import DashboardAutorun from '../../autoruns/DashboardAutorun'
import LogoutButton from '../components/accounts/Auths/Logout'

type renderNavProps = { label: string, link: string, icon: string }

@observer
export default class DashBoard extends PureComponent {
  props: Object;

  @observable isOpen = false

  @action toggleCanvas = () => {
    this.isOpen = !this.isOpen
  }

  links: renderNavProps[] = [
    {
      label: 'Admin Dashboard',
      link: '/blkkskknhdd/dashboard',
      icon: 'dashboard'
    },
    {
      label: 'Packages',
      link: '/blkkskknhdd/packages',
      icon: 'view_list'
    },
    {
      label: 'Withdrawals',
      link: '/blkkskknhdd/withdrawals',
      icon: 'view_list'
    },
    {
      label: 'Donations',
      link: '/blkkskknhdd/donations',
      icon: 'view_list'
    }
  ]

  renderNav = (navProps: renderNavProps) => {
    const { label, link, icon } = navProps
    return (
      <li key={label} className={s({ 'active': store.User.currentRoute.path === link })}>
        <a href={link} className={s('link')}>
          <i className='material-icons'>{icon}</i>
          <p>{label}</p>
        </a>
      </li>
    )
  }

  componentDidMount () {
    DashboardAutorun.start()
  }

  render () {
    const { content } = this.props
    const { User } = store
    if (!User.isKanye) {
      FlowRouter.go('/')
    }
    return (
      <Provider store={store}>
        <OffCanvas width={260} transitionDuration={300} isMenuOpened={this.isOpen} position='right'>
          <OffCanvasMenu>
            <p>Placeholder content.</p>
            <ul>
              <li>Link 1</li>
              <li>Link 2</li>
              <li>Link 3</li>
              <li>Link 4</li>
              <li>Link 5</li>
              <li><a href='#' onClick={this.toggleCanvas}>Toggle Menu</a></li>
            </ul>
          </OffCanvasMenu>
          <OffCanvasBody >
            <main className={s('wrapper')}>
              <meta name='viewport' content='width=device-width, initial-scale=1' />
              <title>Peer Donation</title>
              <div data-color='purple' className={s('sidebar')}>
                <div className={s('logo')}>
                  <a href='/' className={s('simple-text', 'flex', 'jc-sa')}>
                    <div className={s('logo-img')}>
                      <img src='/logo.png' alt='logo' />
                    </div>
                    Peer Donation
                  </a>
                </div>
                <div className={s('sidebar-wrapper')}>
                  <ul className={s('navv')}>
                    {this.links.map(this.renderNav)}
                    <li>
                      <LogoutButton className={s('nav-item', 'is-primary', 'is-tab')}>
                        <i className='material-icons'>apps</i>
                        <p>Logout</p>
                      </LogoutButton>
                    </li>
                  </ul>
                </div>
              </div>
              <div className={s('mainContent', 'main-panel')}>
                <nav className={s('nav', 'is-absolute', 'is-transparent', 'top')}>
                  <div className={s('container', 'jc-e-mobile', 'flex-mobile')}>
                    <span onClick={this.toggleCanvas} className={s('nav-toggle', { 'is-active': this.isOpen })}>
                      <span />
                      <span />
                      <span />
                    </span>
                  </div>
                </nav>
                {content}
              </div>
            </main>
          </OffCanvasBody>
        </OffCanvas>
      </Provider>
    )
  }
}
// className={styles.bodyClass} style={{fontSize: '30px'}}
