import React, { PropTypes, PureComponent } from 'react'
import { observer, Provider } from 'mobx-react'
import { observable, action } from 'mobx'
import cn from 'classnames/bind'
import { OffCanvas, OffCanvasMenu, OffCanvasBody } from 'react-offcanvas'

import store from '../../store'
import styl from '../stylus/index.styl'
import DashboardAutorun from '../../autoruns/DashboardAutorun'
import LogoutButton from '../components/accounts/Auths/Logout'

const s = cn.bind(styl)

@observer
export default class DashBoard extends PureComponent {
  static propTypes = {
    content: PropTypes.element.isRequired
  }


  @observable isOpen = false

  @action toggleCanvas = (status) => {
    if (typeof status !== 'undefined') {
      this.isOpen = !!status
    } else {
      this.isOpen = !this.isOpen
    }
  }

  componentDidMount () {
    DashboardAutorun.start()
  }

  links = [
    {
      label: 'Dashboard',
      link: '/account/dashboard',
      icon: 'dashboard'
    },
    {
      label: 'Transaction History',
      link: '/account/transaction-history',
      icon: 'history'
    },
    {
      label: 'Profile',
      link: '/account/profile',
      icon: 'person'
    },
    {
      label: 'Packages',
      link: '/account/packages',
      icon: 'view_list'
    }
  ]

  renderNav = ({ label, link, icon }) => (
    <li key={label} className={s({ 'active': store.User.currentRoute.path === link })}>
      <a onClick={() => this.toggleCanvas(false)} href={link} className={s('link')}>
        <i className='material-icons'>{icon}</i>
        <p>{label}</p>
      </a>
    </li>
  )

  render () {
    const { content } = this.props
    return (
      <Provider store={store}>
        <OffCanvas width={260} transitionDuration={300} isMenuOpened={this.isOpen} position='right'>
          <OffCanvasMenu />
          <OffCanvasBody>
            <main className={s('wrapper')}>
              <meta name='viewport' content='width=device-width, initial-scale=1' />
              <title>Peer Donation</title>
              <div data-color='purple' className={s('sidebar')}>
                <div className={s('logo')}>
                  <a href='/' className={s('simple-text', 'flex', 'jc-sa')}>
                    <div className={s('logo-img')}>
                      <img src='/logo.png' alt='logo' />
                    </div>
                    Peer Donation
                  </a>
                </div>
                <div className={s('sidebar-wrapper')}>
                  <ul className={s('navv')}>
                    {this.links.map(this.renderNav)}
                    <li>
                      <LogoutButton className={s('nav-item', 'is-primary', 'is-tab')}>
                        <i className='material-icons'>apps</i>
                        <p>Logout</p>
                      </LogoutButton>
                    </li>
                  </ul>
                </div>
              </div>
              <div className={s('mainContent', 'main-panel')}>
                <nav className={s('nav', 'is-absolute', 'is-transparent', 'top')}>
                  <div className={s('container', 'jc-e-mobile', 'flex-mobile', 'is-fullwudth')}>
                    <span
                      onClick={() => this.isOpen ? this.toggleCanvas(false) : this.toggleCanvas(true)}
                      className={s('nav-toggle', 'is-hidden-widescreen', { 'is-active': this.isOpen })}
                    >
                      <span />
                      <span />
                      <span />
                    </span>
                  </div>
                </nav>
                {content}
              </div>
            </main>
          </OffCanvasBody>
        </OffCanvas>
      </Provider>
    )
  }
}
