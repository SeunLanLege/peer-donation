import { FlowRouter } from 'meteor/kadira:flow-router'
import React from 'react'
import { mount, withOptions } from 'react-mounter'

//  Other Route Groups
import './Accounts'
import './Auths'
import './Kanye'

//  Layouts
import MainLayout from '../../../ui/layouts/MainLayout'

//  Pages
import Home from '../../../ui/components/home/index'
import { HowItWorks } from '../../../ui/components/home/HowItWorks'

export const Mount = withOptions({
  rootId: '_imports_ui_stylus__index__react-root'
}, mount)

FlowRouter.route('/', {
  name: 'homepage',
  action () {
    Mount(MainLayout, { content: <Home /> })
  }
})

FlowRouter.route('/howItWorks', {
  name: 'homepage',
  action () {
    Mount(MainLayout, { content: <HowItWorks /> })
  }
})



