import React from 'react'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Meteor } from 'meteor/meteor'
import { Bert } from 'meteor/themeteorchef:bert'
import store from '../../../store'

import { Mount } from './index'
import MainLayout from '../../../ui/layouts/MainLayout'
import Register from '../../../ui/components/accounts/Auths/Register'
import VerifyEmail from '../../../ui/components/accounts/Auths/VerifyEmail'
import Login from '../../../ui/components/accounts/Auths/Login'
import ForgotPassword from '../../../ui/components/accounts/Auths/ForgotPassword'
import ResetPasswordUI from '../../../ui/components/accounts/Auths/ResetPassword'

const Auths = FlowRouter.group({
  triggersEnter: [(context, redirect) => {
    if (Meteor.userId()) {
      if (store.User.isKanye) {
        redirect('/blkkskknhdd/dashboard')
      } else {
        Bert.alert('You\'re already signed in!', 'danger', 'growl-top-right')
        redirect('/account/dashboard')
      }
    }
  } ]
})

Auths.route('/verify-account/:token', {
  name: 'verifyEmail',
  action (params) {
    Mount(MainLayout, { content: <VerifyEmail {...params} /> })
  }
})

Auths.route('/login', {
  name: 'login',
  action () {
    Mount(MainLayout, {
      content: <Login />
    })
  }
})

Auths.route('/register', {
  name: 'register',
  action () {
    Mount(MainLayout, {
      content: <Register />
    })
  }
})

Auths.route('/forgot-password', {
  name: 'forgotPassword',
  action () {
    Mount(MainLayout, {
      content: <ForgotPassword />
    })
  }
})

Auths.route('/reset-password/:token', {
  name: 'resetPassword',
  action (params) {
    Mount(MainLayout, {
      content: <ResetPasswordUI {...params} />
    })
  }
})
