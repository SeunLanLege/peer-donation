import React from 'react'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Meteor } from 'meteor/meteor'
import { Mount } from './index'
import store from '../../../store'

import Dashboard from '../../../ui/components/accounts/Dashboard/Dashboard/index'
import Packages from '../../../ui/components/accounts/Dashboard/Packages/index'
import Profile from '../../../ui/components/accounts/Dashboard/Profile/index'
import History from '../../../ui/components/accounts/Dashboard/History/index'

//  Accounts Layout
import AccountsLayout from '../../../ui/layouts/DashboardLayout'

const Accounts = FlowRouter.group({
  prefix: '/account',
  triggersEnter: [
    (context, redirect) => {
      if (Meteor.userId()) {
        if (store.User.isKanye) {
          redirect('/blkkskknhdd/dashboard')
        }
      } else {
        redirect('/login')
      }
    }
  ]
})

Accounts.route('/dashboard', {
  action () {
    Mount(AccountsLayout, { content: <Dashboard /> })
  }
})

Accounts.route('/packages', {
  action () {
    Mount(AccountsLayout, { content: <Packages /> })
  }
})

Accounts.route('/profile', {
  action () {
    Mount(AccountsLayout, { content: <Profile /> })
  }
})

Accounts.route('/transaction-history', {
  action () {
    Mount(AccountsLayout, { content: <History /> })
  }
})
