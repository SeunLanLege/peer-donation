//  @flow
import React from 'react'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Mount } from './index'
import store from '../../../store'

//  Accounts Layout
import AdminLayout from '../../../ui/layouts/AdminLayout'
import { Dashboard } from '../../../ui/components/Kanye/Dashboard/index'
import { DonationPackagesUI } from '../../../ui/components/Kanye/Packages/index'
import { BeneficiariesUI } from '../../../ui/components/Kanye/Beneficiaries/index'
import { DonorsUI } from '../../../ui/components/Kanye/Donors/index'

const Accounts = FlowRouter.group({
  prefix: '/blkkskknhdd',
  triggersEnter: [
    (context, redirect) => {
      if (!store.User.isKanye) {
        redirect('/login')
      }
    }
  ]
})

Accounts.route('/dashboard', {
  action () {
    Mount(AdminLayout, { content: <Dashboard /> })
  }
})

Accounts.route('/packages', {
  action () {
    Mount(AdminLayout, { content: <DonationPackagesUI /> })
  }
})

Accounts.route('/withdrawals', {
  action () {
    Mount(AdminLayout, { content: <BeneficiariesUI /> })
  }
})

Accounts.route('/donations', {
  action () {
    Mount(AdminLayout, { content: <DonorsUI /> })
  }
})
